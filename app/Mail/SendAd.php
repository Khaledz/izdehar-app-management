<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Ad;

class SendAd extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $ad;
    public $receiver;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Ad $ad, $receiver)
    {
        $this->ad = $ad;
        $this->receiver = $receiver;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(count($this->ad->attachments) > 0)
        {
            return $this->view('emails.ad')
            ->subject($this->ad->title .' - ' . config('app.name'))
            ->attach(asset('storage/ads/' .$this->ad->attachments->first()->name));
        }else{
            return $this->view('emails.ad')
            ->subject($this->ad->title .' - ' . config('app.name'));
        }
    }
}
