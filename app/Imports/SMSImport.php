<?php

namespace App\Imports;

use App\Models\Client;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToArray;

class SMSImport implements ToArray
{
    public function array(array $rows)
    {
        session()->forget('phones');

        unset($rows[0]);
        foreach ($rows as $row) 
        {
            if($row[4] != null && $row[1] != null)
            {
                $data['name'][] = $row[1];
                $data['phone'][] = $row[4];
            }
        }

        session(['phones' => $data]);
    }
}

