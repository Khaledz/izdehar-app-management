<?php

namespace App\Imports;

use App\Models\Client;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ClientImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows->slice(1) as $row) 
        {
            Client::create([
                'name' => $row[1],
                'phone' => $row[4],
                'position' => $row[3],
                'email' => $row[6]
            ]);
        }
    }
}

