<?php

namespace App\Imports;

use App\Models\Client;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToArray;

class EmailImport implements ToArray
{
    public function array(array $rows)
    {
        session()->forget('emails');

        unset($rows[0]);
        foreach ($rows as $row) 
        {
            if($row[6] != null && $row[1] != null)
            {
                $data['name'][] = $row[1];
                $data['email'][] = $row[6];
            }
        }

        session(['emails' => $data]);
    }
}

