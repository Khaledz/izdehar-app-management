<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Ad;

class AdCreated implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $ad;
    public $request;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Ad $ad, $request)
    {
        $this->ad = $ad;
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        return event(new \App\Events\AdCreated($this->ad, $this->request));
    }
}
