<?php

namespace App\Listeners;

use App\Events\AdCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendAdFacebook implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdCreated  $event
     * @return void
     */
    public function handle(AdCreated $event)
    {
        foreach($event->request['channels'] as $channel)
        {
            if($channel == 1)
            {
                // find what account you want?
                foreach($event->request['accounts'] as $account)
                {
                    $target = \App\Models\Account::find($account);
                    if($target->account_provider_id == 1)
                    {
                        $this->doFacebook($event, $target);
                    }
                }
            }
        }
        
    }

    private function doFacebook($event, $target)
    {
        $fb = new \Facebook\Facebook([
            'app_id' => $target->app_id,
            'app_secret' => $target->app_secret,
            'default_graph_version' => 'v3.2',
            'default_access_token' => $target->token, // optional
        ]);

        try {
        if(count($event->ad->attachments) > 0)
        {
            $array =  array(
                'access_token' => $target->token,
                'message' => $event->ad->description,
                'source' => asset('storage/ads/' .$event->ad->attachments->first()->name)
            );
        }else{
            $array =  array(
                'access_token' => $target->token,
                'message' => $event->ad->description,
            );
        }
    
        $response = $fb->post('/'.$target->page_id .'/feed',$array);
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            \App\Models\QueueError::create(['channel' => 'Facebook', 'error' => 'Graph returned an error: ' . $e->getMessage(), 'ad_id' => $event->ad->id]);
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            \App\Models\QueueError::create(['channel' => 'Facebook', 'error' => 'Facebook SDK returned an error: ' . $e->getMessage(), 'ad_id' => $event->ad->id]);
        }
    }

    
}
