<?php

namespace App\Listeners;

use App\Events\AdCreated;
use App\Mail\SendAd;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendAdEmails implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdCreated  $event
     * @return void
     */
    public function handle(AdCreated $event)
    {
        if($event->ad->receiver->emails != null)
        {
            foreach($event->ad->receiver->emails as $receiver)
            {
                Mail::to('devkhaledz@gmail.com')->queue(new SendAd($event->ad, $receiver));
            }
        }
    }
}
