<?php

namespace App\Listeners;

use App\Events\AdCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendAdInstagram implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdCreated  $event
     * @return void
     */
    public function handle(AdCreated $event)
    {
        // check if instgram is requested
        foreach($event->request['channels'] as $channel)
        {
            if($channel == 3)
            {
                // find what account you want?
                foreach($event->request['accounts'] as $account)
                {
                    $target = \App\Models\Account::find($account);
                    if($target->account_provider_id == 3)
                    {
                        /////// CONFIG ///////
                        $username = $target->username;
                        $password = $target->password;
                        $debug = false;
                        $truncatedDebug = false;
                        //////////////////////
                        /////// MEDIA ////////
                        if(count($event->ad->attachments) > 0)
                        {
                            $photoFilename = $event->ad->attachments->first()->name;
                        }
                        
                        $captionText = $event->ad->description;
                        //////////////////////
                        \InstagramAPI\Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;
                        $ig = new \InstagramAPI\Instagram($debug, $truncatedDebug, [
                            'storage'    => 'file',
                            'basefolder' => asset('storage/ads'),
                        ]);
                        try {
                            $ig->login($username, $password);
                        } catch (\Exception $e) {
                            echo 'Something went wrong: '.$e->getMessage()."\n";
                            \App\Models\QueueError::create(['channel' => 'Instagram', 'error' => 'Something went wrong: '.$e->getMessage(), 'ad_id' => $event->ad->id]);
                        }
                        try {
                            // The most basic upload command, if you're sure that your photo file is
                            // valid on Instagram (that it fits all requirements), is the following:
                            // $ig->timeline->uploadPhoto($photoFilename, ['caption' => $captionText]);
                            // However, if you want to guarantee that the file is valid (correct format,
                            // width, height and aspect ratio), then you can run it through our
                            // automatic photo processing class. It is pretty fast, and only does any
                            // work when the input file is invalid, so you may want to always use it.
                            // You have nothing to worry about, since the class uses temporary files if
                            // the input needs processing, and it never overwrites your original file.
                            //
                            // Also note that it has lots of options, so read its class documentation!
                            if(count($event->ad->attachments) > 0)
                            {
                                $photo = new \InstagramAPI\Media\Photo\InstagramPhoto($photoFilename);
                                $ig->timeline->uploadPhoto($photo->getFile(), ['caption' => $captionText]);
                            }
                            
                        } catch (\Exception $e) {
                            echo 'Something went wrong: '.$e->getMessage()."\n";
                            \App\Models\QueueError::create(['channel' => 'Instagram', 'error' => 'Something went wrong: '.$e->getMessage(), 'ad_id' => $event->ad->id]);

                        }
                    }
                }
            }
        }
    }
}
