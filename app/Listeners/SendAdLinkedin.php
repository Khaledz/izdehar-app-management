<?php
namespace App\Listeners;

ini_set('memory_limit', '-1');
use App\Events\AdCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use LinkedIn\Client;
use LinkedIn\Scope;
use LinkedIn\AccessToken;

class SendAdLinkedin implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdCreated  $event
     * @return void
     */
    public function handle(AdCreated $event)
    {
        foreach($event->request['channels'] as $channel)
        {
            if($channel == 4)
            {
                // find what account you want?
                foreach($event->request['accounts'] as $account)
                {
                    $target = \App\Models\Account::find($account);
                    if($target->account_provider_id == 4)
                    {
                        // session(['event' => $event, 'target' => $target,
                        // 'linkedin_username' => $target->username, 'linkedin_password' => $target->password]);
                        $client = new Client(
                            $target->username,
                            $target->password
                        );

                        try {

                        $accessToken = new AccessToken($target->token, $target->expire);
// set token for client
                        $client->setAccessToken($accessToken);
                        if(count($event->ad->attachments) > 0)
                        {
                            $share = $client->post(
                                'people/~/shares',
                                [
                                    'comment' => $event->ad->description,
                                    'content' => [
                                        'title' => $event->ad->title,
                                        'description' =>  $event->ad->description,
                                        'submitted-url' => asset('storage/ads/' .$event->ad->attachments->first()->name),
                                        'submitted-image-url' => asset('storage/ads/' .$event->ad->attachments->first()->name),
                                    ],
                                    'visibility' => [
                                        'code' => 'anyone'
                                    ]
                                ]
                            );
                            dd($share);
                        }else{
                            $share = $client->post(
                                'people/~/shares',
                                [
                                    'comment' => $event->ad->description,
                                    'visibility' => [
                                        'code' => 'anyone'
                                    ]
                                ]
                            );
                        }
                    } catch (\LinkedIn\Exception $exception) {
                        // in case of failure, provide with details
                        \App\Models\QueueError::create(['channel' => 'Linkedin', 'error' => 'Something went wrong: '.$exception->getMessage(), 'ad_id' => $event->ad->id]);

                    }
                    }
                }
            }
        }
    }

    private function doWork($event, $target)
    {
        var_dump('send linkedin');

        // session(['event' => $event, 'target' => $target,
        // 'linkedin_username' => $target->username, 'linkedin_password' => $target->password]);
        $client = new Client(
            $target->username,
            $target->password
        );
        if (isset($_GET['code'])) { // we are returning back from LinkedIn with the code
            if (isset($_GET['state']) &&  // and state parameter in place
                isset($_SESSION['state']) && // and we have have stored state
                $_GET['state'] === $_SESSION['state'] // and it is our request
            ) {
                try {
                    // you have to set initially used redirect url to be able
                    // to retrieve access token
                    $client->setRedirectUrl($_SESSION['redirect_url']);
                    // retrieve access token using code provided by LinkedIn
                    $accessToken = $client->getAccessToken($_GET['code']);
                    // perform api call to get profile information
                    // $profile = $client->get(
                    //     'people/~:(id,email-address,first-name,last-name)'
                    // );
                    // dd($profile); // print profile information
                    // return redirect(route('ad.index'))
                    // ->with('message', 'Your record has been saved successfully.');
                    $share = $client->post(
                        'people/~/shares',
                        [
                            // 'comment' => 'Checkout this amazing PHP SDK for LinkedIn!',
                            'content' => [
                                'title' => 'title',
                                'description' => 'description',
                                // 'submitted-url' => 'https://github.com/zoonman/linkedin-api-php-client',
                                'submitted-image-url' => 'https://github.com/fluidicon.png',
                            ],
                            'visibility' => [
                                'code' => 'anyone'
                            ]
                        ]
                    );
                    // pp($share);
                    // set sandboxed company page id to work with
                    // https://www.linkedin.com/company/devtestco
                    // $companyId = '2414183';
                    // h1('Company information');
                    // $companyInfo = $client->get('companies/' . $companyId . ':(id,name,num-followers,description)');
                    // pp($companyInfo);
                    // h1('Sharing on company page');
                    // $companyShare = $client->post(
                    //     'companies/' . $companyId . '/shares',
                    //     [
                    //         'comment' =>
                    //             sprintf(
                    //                 '%s %s just tried this amazing PHP SDK for LinkedIn!',
                    //                 $profile['firstName'],
                    //                 $profile['lastName']
                    //             ),
                    //         'content' => [
                    //             'title' => 'PHP Client for LinkedIn API',
                    //             'description' => 'OAuth 2 flow, composer Package',
                    //             'submitted-url' => 'https://github.com/zoonman/linkedin-api-php-client',
                    //             'submitted-image-url' => 'https://github.com/fluidicon.png',
                    //         ],
                    //         'visibility' => [
                    //             'code' => 'anyone'
                    //         ]
                    //     ]
                    // );
                    // pp($companyShare);
                    
                    /*
                    // Returns {"serviceErrorCode":100,"message":"Not enough permissions to access media resource","status":403}
                    // You have to be whitelisted or so by LinkedIn
                    $filename = './demo.jpg';
                    $client->setApiRoot('https://api.linkedin.com/');
                    $mp = $client->upload($filename);
                    */
                } catch (\LinkedIn\Exception $exception) {
                    // in case of failure, provide with details
                    // pp($exception);
                    // pp($_SESSION);
                }
                // echo '<a href="/">Start over</a>';
            } else {
                // normally this shouldn't happen unless someone sits in the middle
                // and trying to override your state
                // or you are trying to change saved state during linking
                // echo 'Invalid state!';
                // pp($_GET);
                // pp($_SESSION);
                // echo '<a href="/">Start over</a>';
            }
        } elseif (isset($_GET['error'])) {
            // if you cancel during linking
            // you will be redirected back with reason
            // pp($_GET);
            // echo '<a href="/">Start over</a>';
        }
    }
}
