<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\AdCreated' => [
            'App\Listeners\SendAdFacebook',
            'App\Listeners\SendAdEmails',
            'App\Listeners\SendAdSMS',
            'App\Listeners\SendAdInstagram',
            'App\Listeners\SendAdTwitter',
            'App\Listeners\SendAdLinkedin',
        ],
        'App\Events\PaymentCreated' => [
            'App\Listeners\SendPaymentEmailToAccountant',
        ],
        'App\Events\OfferCreated' => [
            'App\Listeners\SendOfferEmailToAccountant',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
