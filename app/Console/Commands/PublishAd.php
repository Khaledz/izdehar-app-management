<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class PublishAd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'publish:ad';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command check if any ads ready is to publish.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach(\App\Models\Ad::where('datetime', '!=', null)->where('is_publish', 0)->get() as $ad)
        {
            $dateTime = Carbon::createFromFormat('Y-m-d H:i', $ad->datetime)->toDateTimeString();
            // if the current date is bigger than database.
            if(Carbon::now()->format('Y-m-d H:i') > $dateTime)
            {
                $ad->is_publish = 1;
                $ad->save();

                \Artisan::call('queue:work', [ 'database',
                    '--queue' => 'ad-'. $ad->id
                ]);
                \Artisan::call('queue:work');
            }
        }
    }
}
