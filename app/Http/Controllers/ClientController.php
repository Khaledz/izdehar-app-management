<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Attachment;
use App\Models\CustomerType;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::latest()->get();

        return view('clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = CustomerType::pluck('name', 'id');

        return view('clients.create', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
            ['name' => 'required', 'phone' => 'required']
        );

        $client = Client::create($request->all());

        return redirect(route('client.index'))
                ->with('message', 'Your record has been saved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $Client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $Client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $Client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return view('clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $Client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $this->validate($request, 
            ['title' => 'required', 'desription' => 'required']
        );

        $client->update($request->all());

        return redirect(route('client.index'))
                ->with('message', 'Your record has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $Client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();

        return redirect(route('client.index'))
                ->with('message', 'Your record has been deleted successfully.');
    }

    public function import()
    {
        return view('clients.import');
    }

    public function import_store(Request $request)
    {
        if ($request->hasFile('excel_file'))
        {
            $request->excel_file->store('public');
            $attachment = Attachment::create(['name' => $request->excel_file->hashName(), 'orginal_name' => $request->excel_file->getClientOriginalName(), 'extention' => $request->excel_file->extension(), 'user_id' => auth()->user()->id]);
            \Excel::import(new \App\Imports\ClientImport, $request->excel_file->hashName(), 'public');
        }
        return redirect(route('client.index'))
                ->with('message', 'The excel file has been saved successfully.'); 
    }
}
