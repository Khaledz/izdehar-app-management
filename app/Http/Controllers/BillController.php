<?php

namespace App\Http\Controllers;

use App\Models\Bill;
use App\Models\BillType;
use App\Models\Client;
use App\Models\Offer;
use App\Models\Company;
use Illuminate\Http\Request;
use Carbon\Carbon;
class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bills = Bill::latest()->get();

        return view('bills.index', compact('bills'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = BillType::pluck('name', 'id');
        $clients = Client::pluck('name', 'id');
        $companies = Company::pluck('name', 'id');
        $offers = Offer::latest()->pluck('title', 'id');

        return view('bills.create', compact('types', 'clients', 'companies', 'offers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, 
        //     ['title' => 'required', 'description' => 'required']
        // );
        $lastBillNumber = Bill::latest()->first()->bill_number;

        $bill = Bill::create(array_merge($request->except('client_id', 'company_id'),
         ['bill_number' => $lastBillNumber + 1, 'date' => Carbon::now()]));
        
         if($request->bill_type_id == 1)
        {
            $company = Company::find($request->company_id);
            $bill->update(['model' => 'App\Models\Company', 'model_id' => $company->id]);
            // $bill->companies()->save($company);
        }

        if($request->bill_type_id == 2)
        {
            $client = Client::find($request->client_id);
            $client->bills()->save($bill);
        }
        
        
        return redirect(route('bill.index'))
                ->with('message', 'Your record has been saved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bill  $ad
     * @return \Illuminate\Http\Response
     */
    public function show(Bill $ad)
    {
        return view('bills.show', compact('ad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bill  $ad
     * @return \Illuminate\Http\Response
     */
    public function edit(Bill $ad)
    {
        $channels = BillChannel::get();

        return view('bills.edit', compact('ad', 'channels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bill  $ad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bill $ad)
    {
        $this->validate($request, 
            ['title' => 'required', 'desription' => 'required']
        );

        $ad->update($request->only('title', 'desription'));
        $ad->channels()->sync($request->channels);

        return redirect(route('ad.index'))
                ->with('message', 'Your record has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bill  $ad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bill $ad)
    {
        $ad->delete();

        return redirect(route('ad.index'))
                ->with('message', 'Your record has been deleted successfully.');
    }
}
