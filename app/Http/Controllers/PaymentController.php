<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\Offer;
use App\Models\Bill;
use Illuminate\Http\Request;
use Carbon\Carbon;
class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = Payment::latest()->get();

        return view('payments.index', compact('payments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bills = Bill::pluck('bill_number', 'id');

        return view('payments.create', compact('bills'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, 
        //     ['title' => 'required', 'description' => 'required']
        // );
        $lastPaymentNumber = Payment::latest()->first()->payment_number;

        $payment = Payment::create(array_merge($request->except('client_id', 'company_id'),
         ['payment_number' => $lastPaymentNumber + 1, 'date' => Carbon::now()]));
        
         if($request->payment_type_id == 1)
        {
            $company = Company::find($request->company_id);
            $payment->update(['model' => 'App\Models\Company', 'model_id' => $company->id]);
            // $payment->companies()->save($company);
        }

        if($request->payment_type_id == 2)
        {
            $client = Client::find($request->client_id);
            $client->payments()->save($payment);
        }
        
        
        return redirect(route('payment.index'))
                ->with('message', 'Your record has been saved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Payment  $ad
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $ad)
    {
        return view('payments.show', compact('ad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Payment  $ad
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $ad)
    {
        $channels = PaymentChannel::get();

        return view('payments.edit', compact('ad', 'channels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Payment  $ad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $ad)
    {
        $this->validate($request, 
            ['title' => 'required', 'desription' => 'required']
        );

        $ad->update($request->only('title', 'desription'));
        $ad->channels()->sync($request->channels);

        return redirect(route('ad.index'))
                ->with('message', 'Your record has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Payment  $ad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $ad)
    {
        $ad->delete();

        return redirect(route('ad.index'))
                ->with('message', 'Your record has been deleted successfully.');
    }
}
