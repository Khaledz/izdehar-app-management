<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\AdChannel;
use App\Models\AdReceiver;
use App\Models\Attachment;
use App\Events\AdCreated;
use Illuminate\Http\Request;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads = Ad::latest()->get();

        return view('ads.index', compact('ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $channels = AdChannel::get();
        $emails = \App\Models\Client::pluck('email', 'email');
        $phones = \App\Models\Client::pluck('phone', 'phone');

        $attachments = Attachment::where('model', null)->latest()->pluck('orginal_name', 'id');

        return view('ads.create', compact('channels', 'attachments', 'emails', 'phones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $this->validate($request, 
            ['title' => 'required', 'description' => 'required']
        );

        $ad = Ad::create($request->only('title', 'description'));
        $ad_receivers = AdReceiver::create(array_merge(
            ['ad_id' => $ad->id, 'emails' => $request->emails, 'phones' => $request->phones]
        ));
        $ad->channels()->attach($request->channels);

        // if has file?
        if ($request->hasFile('attach'))
        {
            $request->attach->store('public/ads');
            $attachment = Attachment::create(['name' => $request->attach->hashName(), 'orginal_name' => $request->attach->getClientOriginalName(), 'extention' => $request->attach->extension(), 'user_id' => auth()->user()->id]);
            $ad->attachments()->save($attachment);
        }

        // get the emails or phones to send it.

        // if publish == later
        $this->fireEmailAndPhonesEvents($ad, $request);

        if($request->publish == 'later')
        {
            $ad->datetime = $request->datetime;
            $ad->save();
        }
        else
        {
            $ad->datetime = \Carbon\Carbon::now()->format('Y-m-d H:i');
            $ad->save();
        }

        return redirect(route('ad.index'))
                ->with('message', 'Your record has been saved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function show(Ad $ad)
    {
        return view('ads.show', compact('ad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function edit(Ad $ad)
    {
        $channels = AdChannel::get();

        return view('ads.edit', compact('ad', 'channels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ad $ad)
    {
        $this->validate($request, 
            ['title' => 'required', 'desription' => 'required']
        );

        $ad->update($request->only('title', 'desription'));
        $ad->channels()->sync($request->channels);

        return redirect(route('ad.index'))
                ->with('message', 'Your record has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ad $ad)
    {
        $ad->delete();

        return redirect(route('ad.index'))
                ->with('message', 'Your record has been deleted successfully.');
    }
    
    private function fireEmailAndPhonesEvents($ad, $request)
    {
        dispatch(new \App\Jobs\AdCreated($ad, $request->except('attach')))->onQueue('ad-'. $ad->id);
        
        // $event = (new AdCreated($ad, $request->except('attach')))->onQueue('ad-'. $ad->id);
        // event($event);
        
        // event(new AdCreated($ad, $request->except('attach')))->onQueue('ad-'. $ad->id);
        // if($request->emails != null)
        // {
        //     event(new AdCreated($ad));
        // }

        // if($request->phones != null)
        // {
        //     event(new AdCreated($ad));
        // }
    }
}
