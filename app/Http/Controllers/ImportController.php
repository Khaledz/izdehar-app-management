<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Attachment;

class ImportController extends Controller
{
    public function import_emails_store(Request $request)
    {
        $attachment = Attachment::find($request->emailData);

        \Excel::import(new \App\Imports\EmailImport, 'attachments/' . $attachment->name, 'local');
        
        return response()->json(['data' => session('emails')]);
    }

    public function import_phones_store(Request $request)
    {
        $attachment = Attachment::find($request->smsData);
        \Excel::import(new \App\Imports\SMSImport, 'attachments/' . $attachment->name, 'local');

        return response()->json(['data' => session('phones')]);
    }
}
