<?php

namespace App\Http\Controllers;

use App\Models\Trainer;
use App\Models\Attachment;
use App\Models\Course;
use App\Models\Resume;
use Illuminate\Http\Request;

class TrainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trainers = Trainer::latest()->get();

        return view('trainer.index', compact('trainers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('trainer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
            ['name' => 'required', 'phone' => 'required', 'email' => 'required']
        );

        $Trainer = Trainer::create($request->only('name', 'email', 'phone'));

        if ($request->hasFile('resume'))
        {
            $request->resume->store('trainers');
            $attachment = Attachment::create(['name' => $request->resume->hashName(), 'orginal_name' => $request->resume->getClientOriginalName(), 'extention' => $request->resume->extension(), 'user_id' => auth()->user()->id]);
            $resume = new Resume();
            $resume->title = $request->resume->getClientOriginalName();
            $resume->save();
            $resume->attachments()->save($attachment);
            $Trainer->resume()->save($resume);
        }

        return redirect(route('trainer.index'))
                ->with('message', 'Your record has been saved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Trainer  $Trainer
     * @return \Illuminate\Http\Response
     */
    public function show(Trainer $Trainer)
    {
        return view('trainer.show', compact('trainer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Trainer  $Trainer
     * @return \Illuminate\Http\Response
     */
    public function edit(Trainer $Trainer)
    {
        return view('Trainer.edit', compact('Trainer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Trainer  $Trainer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trainer $Trainer)
    {
        $this->validate($request, 
            ['title' => 'required', 'description' => 'required', 'days' => 'required|int', 'trainer_id' => 'exists:trainers,id']
        );

        $Trainer->update($request->all());

        return redirect(route('Trainer.index'))
                ->with('message', 'Your record has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Trainer  $Trainer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trainer $Trainer)
    {
        $Trainer->delete();

        return redirect(route('Trainer.index'))
                ->with('message', 'Your record has been deleted successfully.');
    }

    /**
     * Get the list of trainers by course..
     *
     * @param  \App\Models\Trainer  $Trainer
     * @return \Illuminate\Http\Response
     */
    public function getTrainersByCourse(Request $request)
    {
        return Course::find($request->course_id)->trainers()->get();
    }
}
