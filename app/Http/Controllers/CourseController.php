<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Client;
use App\Models\CourseType;
use App\Models\CourseDate;
use App\Models\Attachment;
use App\Models\Trainer;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::latest()->get();

        return view('course.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::pluck('name', 'id');
        $types = CourseType::pluck('name', 'id');
        $trainers = Trainer::pluck('name', 'id');

        return view('course.create', compact('clients', 'types', 'trainers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
            ['title' => 'required', 'description' => 'required']
        );

        $course = Course::create($request->only('title', 'description', 'course_type_id', 'days', 'city'));
        $course->trainers()->attach($request->trainers);

        // if it's public course
        if($request->course_type_id == 1)
        {
            if($request->datetime != null)
            {
                foreach($request->datetime as $date)
                {
                    $course_date = CourseDate::create(['datetime' => $date, 'course_id' => $course->id]);
                    $course_date->course()->associate($course);
                }
            }
        }

        if ($request->hasFile('design'))
        {
            $request->design->store('courses/' . $course->title);
            $attachment = Attachment::create(['name' => $request->design->hashName(), 'orginal_name' => $request->design->getClientOriginalName(), 'extention' => $request->design->extension(), 'user_id' => auth()->user()->id]);
            $course->attachments()->save($attachment);
        }

        return redirect(route('course.index'))
                ->with('message', 'Your record has been saved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Course  $Course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        return view('course.show', compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Course  $Course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        return view('course.edit', compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Course  $Course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        $this->validate($request, 
            ['title' => 'required', 'description' => 'required', 'days' => 'required|int', 'trainer_id' => 'exists:trainers,id']
        );

        $course->update($request->all());

        return redirect(route('course.index'))
                ->with('message', 'Your record has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Course  $Course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        $course->delete();

        return redirect(route('course.index'))
                ->with('message', 'Your record has been deleted successfully.');
    }
}
