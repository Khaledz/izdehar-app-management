<?php

namespace App\Http\Controllers;

use App\Models\Offer;
use App\Models\Course;
use App\Models\Company;
use App\Models\Trainer;
use Illuminate\Http\Request;
use App\Models\OfferStatus;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offers = offer::latest()->get();

        return view('offer.index', compact('offers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::where('course_type_id', 2)->pluck('title', 'id');
        $companies = Company::pluck('name', 'id');
        $trainers = Trainer::pluck('name', 'id');

        return view('offer.create', compact('courses', 'companies', 'trainers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, 
        //     ['title' => 'required', 'description' => 'required', 'days' => 'required|int', 'trainer_id' => 'exists:trainers,id']
        // );
        $offer = offer::create($request->except(['course_id', 'include_resume_in_offer', 'files']));
        $offer->courses()->attach($request->course_id);
        // $this->generateWord($request);
        // exit;

        return redirect(route('offer.index'))
                ->with('message', 'Your record has been saved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function show(offer $offer)
    {
        return view('offer.show', compact('offer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function edit(offer $offer)
    {
        return view('offer.edit', compact('offer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, offer $offer)
    {
        $this->validate($request, 
            ['title' => 'required', 'description' => 'required', 'days' => 'required|int', 'trainer_id' => 'exists:trainers,id']
        );

        $offer->update($request->all());

        return redirect(route('offer.index'))
                ->with('message', 'Your record has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function destroy(offer $offer)
    {
        $offer->delete();

        return redirect(route('offer.index'))
                ->with('message', 'Your record has been deleted successfully.');
    }

    public function generateWord(Request $request)
    {
        // $course = Course::find($request->course_id);
        // $docx = new CreateDocxFromTemplate(storage_path('offer.docx'));
        // $docx->replaceVariableByHTML('offer_title', 'inline', $request->offer_title);
        // $docx->replaceVariableByHTML('program_overview', 'block', $course->description);
        // $docx->replaceVariableByHTML('course_title', 'block', $course->title);
        // $docx->replaceVariableByHTML('offer_days', 'block', $request->days_number);

        // $docx->createDocx(storage_path('work.docx'));
    }

    public function postTrainerToCourse(Request $request)
    {
        return $request->all();
    }

    public function change_status_get(Offer $offer, Request $request)
    {
        $statuses = OfferStatus::pluck('name', 'id');
        return view('offer.change', compact('offer', 'statuses'));
    }

    public function change_status_post(Offer $offer, Request $request)
    {
        $offer->update(['offer_status_id' => $request->offer_status_id]);

        return redirect(route('offer.index'))
                ->with('message', 'Your record has been updated successfully.');
    }
}
