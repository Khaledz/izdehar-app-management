<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdChannel extends Model
{
    public function ads()
    {
        return $this->belongsToMany('App\Models\AdChannel', 'ads_channels');
    }
}
