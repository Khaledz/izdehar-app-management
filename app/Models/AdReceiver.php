<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdReceiver extends Model
{
    protected $guarded = ['id'];

    protected $table = 'ad_receivers';

    protected $casts = ['emails' => 'json', 'names' => 'json', 'phones' => 'json'];

    public function ad()
    {
        return $this->belongsTo('App\Models\Ad');
    }

    public function setEmailsAttribute($value)
    {
        $this->attributes['emails'] = json_encode($value);
        // name-email
        // if($value != null)
        // {
        //     foreach($value as $string)
        //     {
        //         $row = explode('-', $string);
        //         if(isset($row[1]))
        //         {
        //             $data[] = ['name' => $row[0], 'email' => $row[1]];
        //         }
        //     }

        //     if(isset($row[1]))
        //         {
        //     $this->attributes['emails'] = json_encode($data);
        //         }
        // }
    }

    public function setPhonesAttribute($value)
    {
        $this->attributes['phones'] = json_encode($value);
        // name-phone
        // if($value != null)
        // {
        //     foreach($value as $string)
        //     {
        //         $row = explode('-', $string);
        //         if(isset($row[1]))
        //         {
        //             $data[] = ['name' => $row[0], 'phone' => $row[1]];
        //         }
        //     }
        //     if(isset($row[1]))
        //     {
        //     $this->attributes['phones'] = json_encode($data);
        //     }
        // }
    }
}
