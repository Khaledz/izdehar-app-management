<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BillType extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bill_types';

    protected $guarded = ['id'];

    public function bills()
    {
        return $this->hasMany('App\Models\Bill');
    }
}
