<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trainer extends Model
{
    protected $table = 'trainers';

    protected $guarded = ['id'];

    public function model()
    {
        return $this->morphTo();
    }

    public function resume()
    {
        return $this->hasOne('App\Models\Resume');
    }

    public function courses()
    {
        return $this->belongsToMany('App\Models\Course', 'courses_trainers', 'course_id', 'trainer_id');
    }
}
