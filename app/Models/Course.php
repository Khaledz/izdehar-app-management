<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'courses';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
    
    public function model()
    {
        return $this->morphTo();
    }

    public function clients()
    {
        return $this->belongsToMany('App\Models\Client', 'courses_clients', 'course_id', 'client_id');
    }

    public function trainers()
    {
        return $this->belongsToMany('App\Models\Trainer', 'courses_trainers', 'course_id', 'trainer_id');
    }

    public function dates()
    {
        return $this->hasMany('App\Models\CourseDate');
    }

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'model', 'model');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\CourseType', 'course_type_id');
    }
}
