<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QueueError extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'qeue_errors';

    protected $guarded = ['id'];

}
