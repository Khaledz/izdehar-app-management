<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountType extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'account_providers';

    protected $guarded = ['id'];

    public function accounts()
    {
        return $this->hasMany('App\Models\Account', 'account_provider_id');
    }
}
