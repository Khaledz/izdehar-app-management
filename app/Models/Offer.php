<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $casts = [
        'trainer_id' => 'array',
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'offers';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function setOffer_Status_idAttribute($value)
    {
        $this->attributes['offer_status_id'] = 1;
    }

    public function courses()
    {
        return $this->belongsToMany('App\Models\Course', 'offers_courses', 'offer_id', 'course_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\OfferStatus', 'offer_status_id');
    }

    public function trainer()
    {
        return $this->belongsTo('App\Models\Trainer');
    }

    public function payments()
    {
        return $this->hasMany('App\Models\Payment');
    }

    public function bills()
    {
        return $this->hasMany('App\Models\Bill');
    }
}
