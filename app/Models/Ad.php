<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $guarded = ['id'];

    public function channels()
    {
        return $this->belongsToMany('App\Models\AdChannel', 'ads_channels', 'ad_id', 'ad_channel_id');
    }

    public function getPublishedText()
    {
        if($this->is_publish == 1)
        {
            return '<span class="badge badge-success">Yes</span>';
        }
        else 
        {
            return '<span class="badge badge-danger">No ('.$this->datetime.')</span>';
        }
    }

    public function model()
    {
        return $this->morphTo();
    }

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'model', 'model');
    }

    public function receiver()
    {
        return $this->hasOne('App\Models\AdReceiver');
    }
}
