<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cost extends Model
{
    protected $guarded = ['id'];

    public function offer()
    {
        return $this->belongsTo('App\Models\Offer');
    }
}
