<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bills';

    protected $guarded = ['id'];

    // public function model()
    // {
    //     return $this->morphTo();
    // }
    
    public function offer()
    {
        return $this->belongsTo('App\Models\Offer');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\BillType', 'bill_type_id');
    }

    public function clients()
    {
        return $this->morphMany('App\Models\Client', 'model', 'model');
    }

    public function companies()
    {
        return $this->morphMany('App\Models\Company', 'model', 'model');
    }
}
