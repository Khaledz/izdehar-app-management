<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resume extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'resumes';

    public function Trainer()
    {
        return $this->belongsTo('App\Models\Trainer');
    }

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'model', 'model');
    }
}
