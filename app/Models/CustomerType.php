<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerType extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customer_types';   
}