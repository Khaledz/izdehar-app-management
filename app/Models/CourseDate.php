<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseDate extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'course_dates';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function course()
    {
        return $this->belongsTo('App\Models\Course', 'course_id');
    }
}