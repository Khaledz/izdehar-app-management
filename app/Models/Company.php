<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'companies';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function courses()
    {
        return $this->belongsToMany('App\Models\Client', 'courses_clients', 'course_id', 'client_id');
    }
    public function model()
    {
        return $this->morphTo();
    }
    
    // public function bills()
    // {
    //     return $this->morphMany('App\Models\Bill', 'model', 'model');
    // }
}
