<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'attachments';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'orginal_name', 'description', 'extention', 'model', 'model_id', 'user_id'];

    public function model()
    {
        return $this->morphTo();
    }

    public function trainer()
    {
        return $this->belongsTo('App\Models\Trainer');
    }

    public function resume()
    {
        return $this->belongsTo('App\Models\Resume');
    }

    public function ad()
    {
        return $this->belongsTo('App\Models\Ad');
    }

    public function bill()
    {
        return $this->belongsTo('App\Models\Bill');
    }
}
