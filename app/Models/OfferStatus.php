<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfferStatus extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'offer_statuses';

    protected $guarded = ['id'];

    public function offers()
    {
        return $this->hasMany('App\Models\Offer');
    }
}
