<?php

return [

    'telegram' => [
        'api_token' => '',
        'bot_username' => '',
        'channel_username' => '', // Channel username to send message
        'channel_signature' => '', // This will be assigned in the footer of message
        'proxy' => false,   // True => Proxy is On | False => Proxy Off
    ],

    'twitter' => [
        'consurmer_key' => '',
        'consurmer_secret' => '',
        'access_token' => '',
        'access_token_secret' => ''
    ],

    'facebook' => [
        'app_id' => '728667637520578',
        'app_secret' => '26a0d5dbbca2f366d52826fe32827fbd',
        'default_graph_version' => '',
        'page_access_token' => 'EAAKWuCexmMIBAFwCahstU36et9ShxU9U3cwLuqERTgpIxJDM7ZAzNaaIAPLYfhU9eLY4WnN2OjcUe86bVNMmLZCIFFXIz1KGQFp6TZB7itwAmLPVHeYyhBaC5x6EPZCxy07zoqzcJKpUEaTjiLdYvSRuQkSV2tbSuZCFRiOwVma7OXzYVX6HhxJnfOKmWTUuXXcwbETrOB9GZCUe0ka6m9CKsDXwLy6ZB0CHqNUHJ0mvgZDZD'
    ],

    // Set Proxy for Servers that can not Access Social Networks due to Sanctions or ...
    'proxy' => [
        'type' => '',   // 7 for Socks5
        'hostname' => '', // localhost
        'port' => '' , // 9050
        'username' => '', // Optional
        'password' => '', // Optional
    ]
];
