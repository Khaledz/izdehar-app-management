@extends('layouts.app')
@section('title')
    List Companies
@endsection
@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Representative Name</th>
                            <th>Representative Phone</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if($companies->count() > 0)
                        @foreach($companies as $company)
                        <tr>
                            <td>{{ $company->name }}</td>
                            <td>{{ $company->phone }}</td>
                            <td>{{ $company->responsible_name }}</td>
                            <td>{{ $company->responsible_phone }}</td>
                            <td>
                            <a href="{{ route('company.show', $company->id)}}" class="btn btn-xs btn-info"><i class="mdi mdi-eye"></i></a>
                            <a href="{{ route('company.edit', $company->id)}}" class="btn btn-xs btn-secondary"><i class="mdi mdi-pencil"></i></a>
                            <a href="#" class="btn btn-xs btn-danger" onclick="document.getElementById('deleteForm').submit();"><i class="mdi mdi-delete"></i></a>
                            {!! Form::open(['route' => ['company.destroy', $company], 'method' => 'delete', 'id' => 'deleteForm']) !!}
                            {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
                                        
@endsection

@section('script')
<script src="/assets/libs/datatables/jquery.dataTables.js"></script>
<script src="/assets/libs/datatables/dataTables.bootstrap4.js"></script>
<script src="/assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="/assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="/assets/libs/datatables/buttons.html5.min.js"></script>
<script src="/assets/js/pages/datatables.init.js"></script>
@endsection