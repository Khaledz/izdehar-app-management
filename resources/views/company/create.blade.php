@extends('layouts.app')
@section('title')
    Create company
@endsection

@section('content')

<!-- end page title --> 
{!! Form::open(['route' => 'company.store', 'method' => 'post']) !!}
<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <div class="form-group mb-3">
                {!! Form::label('Customer Type') !!}
                {!! Form::select('customer_type_id', $types, null, ['class' => 'select2 form-control']) !!}
            </div>
        
            <div class="form-group mb-3">
                {!! Form::label('Name') !!}
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('Phone') !!}
                {!! Form::text('phone', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('Fax') !!}
                {!! Form::text('fax', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('Representative Name') !!}
                {!! Form::text('responsible_name', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('Representative Phone') !!}
                {!! Form::text('responsible_phone', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('Representative Email') !!}
                {!! Form::text('responsible_email', null, ['class' => 'form-control']) !!}
            </div>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<div class="row">
    <div class="col-12">
        <div class="text-center mb-3">
            <button type="submit" class="btn btn-success waves-effect waves-light">
                <span class="btn-label"><i class="mdi mdi-check-all"></i></span>Save
            </button>
        </div>
    </div> <!-- end col -->
</div>
{!! Form::close() !!}
@endsection
