@extends('layouts.app')
@section('title')
    List Bills
@endsection
@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Bill Number</th>
                            <th>Customer</th>
                            <th>Course</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if($bills->count() > 0)
                        @foreach($bills as $bill)
                        <tr>
                            <td>{{ $bill->bill_number }}</td>
                            <td>
                            @if($bill->model == 'App\Models\Company')
                                {{ \App\Models\Company::find($bill->model_id)->name }}
                            @elseif($bill->model == 'App\Models\Client')
                                {{ \App\Models\Client::find($bill->model_id)->name }}
                            @endif
                            </td>
                            <td>
                            @foreach($bill->offer->courses as $course)
                                {{ $course->title .', ' }}
                            @endforeach
                            </td>
                            <td>
                            <a href="{{ route('bill.show', $bill->id)}}" class="btn btn-xs btn-info"><i class="mdi mdi-eye"></i></a>
                            <a href="{{ route('bill.edit', $bill->id)}}" class="btn btn-xs btn-secondary"><i class="mdi mdi-pencil"></i></a>
                            <a href="#" class="btn btn-xs btn-danger" onclick="document.getElementById('deleteForm').submit();"><i class="mdi mdi-delete"></i></a>
                            {!! Form::open(['route' => ['bill.destroy', $bill], 'method' => 'delete', 'id' => 'deleteForm']) !!}
                            {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
                                        
@endsection

@section('script')
<script src="/assets/libs/datatables/jquery.dataTables.js"></script>
<script src="/assets/libs/datatables/dataTables.bootstrap4.js"></script>
<script src="/assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="/assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="/assets/libs/datatables/buttons.html5.min.js"></script>
<script src="/assets/js/pages/datatables.init.js"></script>
@endsection