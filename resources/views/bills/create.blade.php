@extends('layouts.app')
@section('title')
    Create Bill
@endsection

@section('content')

<!-- end page title --> 
{!! Form::open(['route' => 'bill.store', 'method' => 'post']) !!}
<div class="row">
    <div class="col-lg-12">
        <div class="card-box">

            <div class="form-group mb-3">
                {!! Form::label('Bill Type') !!}
                {!! Form::select('bill_type_id', $types, null, ['class' => 'select2 form-control', 'id' => 'bill_type']) !!}
            </div>

            <div class="form-group mb-3" id="offer">
                {!! Form::label('Offer') !!}
                {!! Form::select('offer_id', $offers, null, ['class' => 'select2 form-control']) !!}
            </div>

            <div class="form-group mb-3" id="client">
                {!! Form::label('Client') !!}
                {!! Form::select('client_id', $clients, null, ['class' => 'select2 form-control']) !!}
            </div>

            <!-- <div class="form-group mb-3" id="company">
                {!! Form::label('Company') !!}
                {!! Form::select('company_id', $companies, null, ['class' => 'select2 form-control']) !!}
            </div> -->

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<div class="row">
    <div class="page-title-box">
        <h4 class="page-title">Payment Details</h4>
    </div>

    <div class="col-lg-12">
        <div class="card-box">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group mb-3">
                        {!! Form::label('Total Amount') !!}
                        {!! Form::text('total', null, ['class' => 'form-control', 'id' => 'total']) !!}
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group mb-3">
                        {!! Form::label('Paid Amount') !!}
                        {!! Form::text('paid_amount', null, ['class' => 'form-control', 'id' => 'paid_amount']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group mb-3">
                        {!! Form::label('Remaining Amount') !!}
                        {!! Form::text('remaining_amount', null, ['class' => 'form-control', 'id' => 'remaining_amount']) !!}
                    </div>
                </div>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<div class="row">
    <div class="col-12">
        <div class="text-center mb-3">
            <button type="submit" class="btn btn-success waves-effect waves-light">
                <span class="btn-label"><i class="mdi mdi-check-all"></i></span>Save & export as pdf
            </button>
        </div>
    </div> <!-- end col -->
</div>
{!! Form::close() !!}
@endsection

@section('script')
<script>
$(document).ready(function() {
    $('#client').hide();
    $('#company').show();
});

$('#bill_type').on('change', function() {
    if($(this).val() == 1)
    {
        $('#company').show();
        $('#offer').show();
        $('#client').hide();
    }

    if($(this).val() == 2)
    {
        $('#company').hide();
        $('#offer').hide();
        $('#client').show();
    }
});
</script>
@endsection