<!-- Navigation Bar-->
<header id="topnav">

<!-- Topbar Start -->
<div class="navbar-custom">
    <div class="container-fluid">
        <ul class="list-unstyled topnav-menu float-right mb-0">

            <li class="dropdown notification-list">
                <!-- Mobile menu toggle-->
                <a class="navbar-toggle nav-link">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <!-- End mobile menu toggle-->
            </li>
            <li class="dropdown notification-list">
                <a class="nav-link dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <i class="fe-bell noti-icon"></i>
                    <span class="badge badge-danger rounded-circle noti-icon-badge">1</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-lg">

                    <!-- item-->
                    <div class="dropdown-item noti-title">
                        <h5 class="m-0">
                            <span class="float-right">
                            
                            </span>Notification
                        </h5>
                    </div>

                    <div class="slimscroll noti-scroll">

                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item active">
                            <div class="notify-icon bg-primary">
                                <i class="mdi mdi-comment-account-outline"></i>
                            </div>
                            <p class="notify-details">Cristina Pride</p>
                            <p class="text-muted mb-0 user-msg">
                                <small>Hi, How are you? What about our next meeting</small>
                            </p>
                        </a>
                    </div>
                </div>
            </li>
            <li>
                <a class="nav-link nav-user mr-0" href="#">
                    <span class="pro-user-name ml-1">
                    <i class="fe-user"></i> {{ (auth()->check()) ? Auth::user()->name : '' }}
                    </span>
                </a>
            </li>

            <li>
                <a class="nav-link nav-user" href="#"  href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <span class="pro-user-name">
                    <i class="fe-log-out"></i>{{ (auth()->check()) ? 'Logout' : '' }} 
                    </span>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </a>
            </li>

        </ul>

        <!-- LOGO -->
        <div class="logo-box">
            <a href="{{ url('/') }}" class="logo text-center">
                <span class="logo-lg">
                    <img src="{{ asset('assets/images/logo.jpg') }}" alt="" height="69">
                    <!-- <span class="logo-lg-text-light">UBold</span> -->
                </span>
                <span class="logo-sm">
                    <!-- <span class="logo-sm-text-dark">U</span> -->
                    <img src="{{ asset('assets/images/logo.jpg') }}" alt="" height="69">
                </span>
            </a>
        </div>

       
    </div> <!-- end container-fluid-->
</div>
<!-- end Topbar -->

<div class="topbar-menu">
    <div class="container-fluid">
        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">
                <li>
                    <a href="{{ url('/') }}">
                        <i class="fe-airplay"></i>Dashboard
                    </a>
                </li>
                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-grid"></i>Social Media <div class="arrow-down"></div>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="{{ route('ad.index' )}}">List Advertisements</a>
                        </li>
                        <li>
                            <a href="{{ route('ad.create' )}}">Create Advertisement</a>
                        </li>
                        <li>
                            <a href="{{ route('social-media-account.index') }}">Manage Accounts</a>
                        </li>
                    </ul>
                </li>
                <!-- <li class="has-submenu">
                    <a href="#">
                        <i class="fe-grid"></i>Advertisements <div class="arrow-down"></div>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="{{ route('ad.index' )}}">List Advertisements</a>
                        </li>
                        <li>
                            <a href="{{ route('ad.create' )}}">Create Advertisement</a>
                        </li>
                    </ul>
                </li> -->
                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-grid"></i>Customers <div class="arrow-down"></div>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="{{ route('company.index' )}}">List Companies</a>
                        </li>
                        <li>
                            <a href="{{ route('company.create' )}}">Create Company</a>
                        </li>
                        <li>
                            <a href="{{ route('client.index' )}}">List Clients</a>
                        </li>
                        <li>
                            <a href="{{ route('client.create' )}}">Create Client</a>
                        </li>
                        <li>
                            <a href="{{ route('client.import')}}">Import Excel Data</a>
                        </li>
                    </ul>
                </li>
                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-grid"></i>Services <div class="arrow-down"></div>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="{{ route('offer.index') }}">List Offers</a>
                        </li>
                        <li>
                            <a href="{{ route('offer.create') }}">Create Offer</a>
                        </li>
                        <li>
                            <a href="{{ route('course.index') }}">List Courses</a>
                        </li>
                        <li>
                            <a href="{{ route('course.create') }}">Create Course</a>
                        </li>
                        <li>
                            <a href="{{ route('trainer.index') }}">Trainers</a>
                        </li>
                        <li>
                            <a href="{{ route('trainer.create') }}">Create Trainer</a>
                        </li>
                    </ul>
                </li>
                
                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-grid"></i>Payments & Bills <div class="arrow-down"></div>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="{{ route('payment.index') }}">List Payments</a>
                        </li>
                        <li>
                            <a href="{{ route('payment.create') }}">Create Payment</a>
                        </li>
                        <li>
                            <a href="{{ route('bill.index') }}">List Bills</a>
                        </li>
                        <li>
                            <a href="{{ route('bill.create') }}">Create Bill</a>
                        </li>
                    </ul>
                </li>
                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-grid"></i>Users <div class="arrow-down"></div>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="{{ route('user.index') }}">List Users</a>
                        </li>
                        <li>
                            <a href="{{ route('user.create') }}">Create User</a>
                        </li>
                    </ul>
                </li>
                <li class="has-submenu">
                    <a href="#">
                        <i class="fe-grid"></i>Files <div class="arrow-down"></div>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="{{ route('attachment.index') }}">List Files</a>
                        </li>
                        <li>
                            <a href="{{ route('attachment.create') }}">Upload File</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- End navigation menu -->

            <div class="clearfix"></div>
        </div>
        <!-- end #navigation -->
    </div>
    <!-- end container -->
</div>
<!-- end navbar-custom -->

</header>
<!-- End Navigation Bar-->