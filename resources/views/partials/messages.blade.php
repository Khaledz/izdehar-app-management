@if ($errors->any())
    @foreach ($errors->all() as $error)
    <div class="alert alert-danger bg-danger text-white border-0" role="alert">
        {{ $error }}
    </div>
    @endforeach
@endif

@if(session('message'))
<div class="alert alert-success bg-success text-white border-0" role="alert">
    {{ session('message') }}
</div>
@endif