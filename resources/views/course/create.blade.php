@extends('layouts.app')
@section('title')
    Create Course
@endsection

@section('content')

<!-- end page title --> 
{!! Form::open(['route' => 'course.store', 'method' => 'post', 'files' => true]) !!}
<div class="row">
    <div class="col-lg-12">
        <div class="card-box">

            <div class="form-group mb-3">
                {!! Form::label('Course Type') !!}
                {!! Form::select('course_type_id', $types, null, ['class' => 'select2 form-control', 'id' => 'courseType']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('Course Title') !!}
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('Course Overview') !!}
                {!! Form::textarea('description', null, ['class' => 'summernote form-control']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('Design') !!}
                {!! Form::file('design', ['class' => 'form-control']) !!}
            </div>

            <div class="form-group mb-3" >
                {!! Form::label('Trainers') !!}
                {!! Form::select('trainers[]', $trainers, null, ['class' => 'select2 form-control select2-multiple', 'multiple' => 'multiple']) !!}
            </div>

            <!-- if it's public course -->
            <div id="publicCourse">
                <div class="form-group mb-3">
                    {!! Form::label('City') !!}
                    {!! Form::text('city', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group mb-3">
                    {!! Form::label('Number of days') !!}
                    {!! Form::text('days', null, ['class' => 'days form-control']) !!}
                </div>

                <div class="dateTime form-group mb-3">
                    {!! Form::label('Date & Time', null, ['class' => 'dateLabel']) !!}
                    <input name="datetime[]" type="text" id="datetime-datepicker" class="datetime-datepicker form-control" placeholder="Date and Time">
                </div>
                <div class="datesList"></div>
            </div>
            <!-- end of public course -->

            
                
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<div class="row">
    <div class="col-12">
        <div class="text-center mb-3">
            <button type="submit" class="btn btn-success waves-effect waves-light">
                <span class="btn-label"><i class="mdi mdi-check-all"></i></span>Save
            </button>
        </div>
    </div> <!-- end col -->
</div>
{!! Form::close() !!}
@endsection

@section('script')
<script>
$(document).ready(function() {
    $('#publicCourse').show();
});

$('#courseType').on('change', function() {
    if($(this).val() == 1)
    {
        $('#publicCourse').show();
    }

    if($(this).val() == 2)
    {
        $('#publicCourse').hide();
    }
});

$('.days').keyup(function(event){
    $('.datesList').html('');

    var $publicCourseDiv = $('.dateTime').clone();
    for(var i = 0; i < $(this).val() - 1; i++)
    {
        // datetime-datepicker
        let number = i + 1;
        
        $('.datesList').append($publicCourseDiv.clone().first());
        $('.datetime-datepicker').flatpickr({
            enableTime: !0,
            dateFormat: "Y-m-d H:i"
        });
    }
});

</script>
@endsection