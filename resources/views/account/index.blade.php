@extends('layouts.app')
@section('title')
    Social Media Accounts
@endsection
@section('content')

<div class="row">
    <div class="col-12">
        <div class="row">
    <!-- Facebook -->
    @foreach($types as $type)
            <div class="col-lg-3">
                <div class="card-box bg-pattern">
                    <div class="text-center">
                        <img src="assets/images/companies/{{ strtolower($type->name) }}.png" alt="logo" class="avatar-xl rounded-circle">
                        <h4 class="mb-2 font-20">{{ ucfirst($type->name) }}</h4>
                    </div>

                    <div class="text-center mb-1">
                        <a href="{{ route('social-media-account.create') }}" class="mt-2 btn btn-md btn-light">Add Account</a>
                    </div>
                    <div class="text-center">
                        @if($type->accounts()->count() > 0)
                        
                            @foreach($type->accounts as $account)
                            @if($account->account_provider_id == 4)
                            @php
                            $client = new \LinkedIn\Client(
        $account->username,
         $account->password
     );
      $client->setRedirectUrl('http://auto.localhost/linkedin');
        // define desired list of scopes
        $scopes = [
            \LinkedIn\Scope::READ_BASIC_PROFILE,
            \LinkedIn\Scope::READ_EMAIL_ADDRESS,
            \LinkedIn\Scope::MANAGE_COMPANY,
            \LinkedIn\Scope::SHARING,
        ];
        $loginUrl = $client->getLoginUrl($scopes); // get url on LinkedIn to start linking
        $_SESSION['state'] = $client->getState(); // save state for future validation
        $_SESSION['redirect_url'] = $client->getRedirectUrl(); // save redirect url for future validation
                            session(['account' => $account]);
                            @endphp
                            <p class="font-20">{{ $account->name }} - <a href="{{ $loginUrl }}">get new token</a></p><br>
                            @else
                            <p class="font-20">{{ $account->name }}</p><br>
                            @endif
                            @endforeach
                        
                        @endif
                    </div>
                </div> <!-- end card-box -->
            </div><!-- end col -->
    @endforeach
        </div>
    </div>
</div>
    

@endsection