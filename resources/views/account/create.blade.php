@extends('layouts.app')
@section('title')
    Create Social Account
@endsection

@section('content')

<!-- end page title --> 
{!! Form::open(['route' => 'social-media-account.store', 'method' => 'post']) !!}
<div class="row">
    <div class="col-lg-12">
    <p class="">To add a new account, 
    you have first to register new application in social media account provider
    to get your credentials.</small>

        <div class="card-box">
            <div class="form-group mb-3">
                {!! Form::label('Account Type') !!}
                {!! Form::select('account_provider_id', $types, null, ['class' => 'form-control', 'id' => 'social-media-account_type']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('Name') !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'total']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('Username') !!}
                {!! Form::text('username', null, ['class' => 'form-control', 'id' => 'total']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('Password') !!}
                {!! Form::text('password', null, ['class' => 'form-control', 'id' => 'total']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('App ID (Facebook Only)') !!}
                {!! Form::text('app_id', null, ['class' => 'form-control', 'id' => 'total']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('App Secret (Facebook Only)') !!}
                {!! Form::text('app_secret', null, ['class' => 'form-control', 'id' => 'total']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('Token (Facebook Only)') !!}
                {!! Form::text('token', null, ['class' => 'form-control', 'id' => 'total']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('Page ID (Facebook Only)') !!}
                {!! Form::text('page_id', null, ['class' => 'form-control', 'id' => 'total']) !!}
            </div>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<div class="row">
    <div class="col-12">
        <div class="text-center mb-3">
            <button type="submit" class="btn btn-success waves-effect waves-light">
                <span class="btn-label"><i class="mdi mdi-check-all"></i></span>Save
            </button>
        </div>
    </div> <!-- end col -->
</div>
{!! Form::close() !!}
@endsection

@section('script')
<script>
</script>
@endsection