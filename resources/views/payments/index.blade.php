@extends('layouts.app')
@section('title')
    List Payments
@endsection
@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Bill Number</th>
                            <th>Customer</th>
                            <th>Total Amount</th>
                            <th>Remaining Amount</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if($payments->count() > 0)
                        @foreach($payments as $payment)
                        <tr>
                            <td>{{ $payment->bill->bill_number }}</td>
                            <td>
                            @if($payment->bill->model == 'App\Models\Company')
                                {{ \App\Models\Company::find($payment->bill->model_id)->name }}
                            @elseif($payment->model == 'App\Models\Client')
                                {{ \App\Models\Client::find($payment->bill->model_id)->name }}
                            @endif
                            </td>
                            <td>SR{{ $payment->total }}</td>
                            <td>SR{{ $payment->remaining_amount }}</td>
                            <td>
                            <a href="{{ route('payment.show', $payment->id)}}" class="btn btn-xs btn-info"><i class="mdi mdi-eye"></i></a>
                            <a href="{{ route('payment.edit', $payment->id)}}" class="btn btn-xs btn-secondary"><i class="mdi mdi-pencil"></i></a>
                            <a href="#" class="btn btn-xs btn-danger" onclick="document.getElementById('deleteForm').submit();"><i class="mdi mdi-delete"></i></a>
                            {!! Form::open(['route' => ['payment.destroy', $payment], 'method' => 'delete', 'id' => 'deleteForm']) !!}
                            {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
                                        
@endsection

@section('script')
<script src="/assets/libs/datatables/jquery.dataTables.js"></script>
<script src="/assets/libs/datatables/dataTables.bootstrap4.js"></script>
<script src="/assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="/assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="/assets/libs/datatables/buttons.html5.min.js"></script>
<script src="/assets/js/pages/datatables.init.js"></script>
@endsection