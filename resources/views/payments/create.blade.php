@extends('layouts.app')
@section('title')
    Create Payment
@endsection

@section('content')

<!-- end page title --> 
{!! Form::open(['route' => 'payment.store', 'method' => 'post']) !!}
<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
<!-- total	paid_amount	first_paid_amount_percentage	remaining_amount	is_paid -->
            <div class="form-group mb-3">
                {!! Form::label('Bill Number') !!}
                {!! Form::select('bill_id', $bills, null, ['class' => 'select2 form-control', 'id' => 'payment_type']) !!}
            </div>

            <div class="form-group mb-3" id="client">
                {!! Form::label('Total Amount') !!}
                {!! Form::text('total', null, ['class' => 'form-control', 'id' => 'total']) !!}
            </div>


        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<div class="row">
    <div class="col-12">
        <div class="text-center mb-3">
            <button type="submit" class="btn btn-success waves-effect waves-light">
                <span class="btn-label"><i class="mdi mdi-check-all"></i></span>Save & export as pdf
            </button>
        </div>
    </div> <!-- end col -->
</div>
{!! Form::close() !!}
@endsection

@section('script')
<script>
$(document).ready(function() {
    $('#client').hide();
    $('#company').show();
});

$('#payment_type').on('change', function() {
    if($(this).val() == 1)
    {
        $('#company').show();
        $('#client').hide();
    }

    if($(this).val() == 2)
    {
        $('#company').hide();
        $('#client').show();
    }
});
</script>
@endsection