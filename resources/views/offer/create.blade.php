@extends('layouts.app')
@section('title')
    Create Offer
@endsection

@section('content')

<!-- end page title --> 
{!! Form::open(['route' => 'offer.store', 'method' => 'post']) !!}
<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <!-- <h4 class="header-title mb-3">Offer Pricing</h4> -->
                <div id="basicwizard">
                    <ul class="nav nav-pills bg-light nav-justified form-wizard-header mb-3">
                        <li class="nav-item" data-target-form="#accountForm">
                            <a href="#target" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                <i class="mdi mdi-account-circle mr-1"></i>
                                <span class="d-none d-sm-inline">Offer Target</span>
                            </a>
                        </li>
                        <li class="nav-item" data-target-form="#accountForm">
                            <a href="#first" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                <i class="mdi mdi-account-circle mr-1"></i>
                                <span class="d-none d-sm-inline">Offer Details</span>
                            </a>
                        </li>
                        <li class="nav-item" data-target-form="#profileForm">
                            <a href="#second" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                <i class="mdi mdi-face-profile mr-1"></i>
                                <span class="d-none d-sm-inline">Offer Cost</span>
                            </a>
                        </li>
                        <li class="nav-item" data-target-form="#otherForm">
                            <a href="#third" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                <i class="mdi mdi-checkbox-marked-circle-outline mr-1"></i>
                                <span class="d-none d-sm-inline">Offer Pricing</span>
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content mb-0 b-0">
                        <div class="tab-pane" id="target">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group mb-3">
                                        {!! Form::label('Company') !!}
                                        {!! Form::select('company_id', $companies, null, ['class' => 'form-control', 'id' => 'companies']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="course-list">
                                <div class="col-md-6" id="col6co">
                                    <div class="form-group mb-3" id="mb3co">
                                        {!! Form::label('Course') !!} 
                                        {!! Form::select('course_id[]', $courses, null, ['class' => 'courses form-control']) !!}
                                    </div>
                                </div>

                                <div class="col-md-6" id="wow">
                                    <div class="form-group mb-3" id="mb3tr">
                                        {!! Form::label('Trainers') !!}
                                        <h5><p class="trainers"></p></h5>
                                        <span class="help-block">
                                        <div class="mb-2 mt-1 ml-1">
                                            <input name="include_resume_in_offer" type="checkbox" checked="checked">
                                                <label>
                                                    Include resume in the offer.
                                                </label>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div id="new-course"></div>
                            <button id="add-course" type="button" class="btn btn-success btn-xs waves-effect waves-light"><i class="mdi mdi-plus"></i>Add new course</button>
                        </div>
                        <div class="tab-pane" id="first">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group mb-3">
                                        {!! Form::label('Title') !!}
                                        {!! Form::text('title', null, ['class' => 'form-control']) !!}
                                    </div>

                                    <div class="form-group mb-3">
                                        {!! Form::label('Number of days') !!}
                                        {!! Form::text('days_number', null, ['class' => 'form-control']) !!}
                                    </div>

                                    <div class="form-group mb-3">
                                        {!! Form::label('Hours per day') !!}
                                        {!! Form::text('hours_per_day', null, ['class' => 'form-control']) !!}
                                    </div>

                                    <div class="form-group mb-3">
                                        {!! Form::label('Location') !!}
                                        {!! Form::text('location', null, ['class' => 'form-control']) !!}
                                    </div>

                                    <div class="form-group mb-3">
                                        {!! Form::label('Services cost') !!}
                                        {!! Form::textarea('service_cost', null, ['class' => 'summernote form-control', 'rows' => 4, 'id' => 'service_cost']) !!}
                                    </div>

                                    <div class="form-group mb-3">
                                        {!! Form::label('Payment Policy') !!}
                                        {!! Form::textarea('payment_policy', null, ['class' => 'summernote form-control', 'rows' => 4, 'id' => 'payment_policy']) !!}
                                    </div>

                                    <div class="form-group mb-3">
                                        {!! Form::label('General Provisions') !!}
                                        {!! Form::textarea('general_provisions', null, ['class' => 'summernote form-control', 'rows' => 4, 'id' => 'general_provisions']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="second">
                            <h4 class="header-title mb-3">Main Cost Inputs</h4>
                            <!-- row (main inputs)-->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group mb-3">
                                        {!! Form::label('Trainer cost per day') !!}
                                        {!! Form::text('trainer_cost_per_day', null, ['class' => 'form-control', 'id' => 'trainer_cost_daily']) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group mb-3">
                                        {!! Form::label('Number of trainees') !!}
                                        {!! Form::text('number_of_trainees', null, ['class' => 'form-control', 'id' => 'number_of_trainees']) !!}
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group mb-3">
                                        {!! Form::label('Number of days') !!}
                                        {!! Form::text('days_number', null, ['class' => 'form-control', 'id' => 'number_of_days']) !!}
                                    </div>
                                </div>
                            </div>
                            <!-- end row (main inputs) -->
                            <div class="dropdown-divider"></div>

                            <!-- Trainer Cost -->
                            <h4 class="header-title mb-3 mt-4">Trainer Cost</h4>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group mb-3">
                                        {!! Form::label('Trainer ticket cost') !!}
                                        {!! Form::text('trainer_ticket_cost', 1000, ['class' => 'form-control', 'id' => 'trainer_ticket_cost']) !!}
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group mb-3">
                                        {!! Form::label('Trainer accommodation') !!}
                                        {!! Form::text('trainer_accommodation', 2400, ['class' => 'form-control', 'id' => 'trainer_accommodation']) !!}
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group mb-3">
                                        {!! Form::label('Trainer transport cost') !!}
                                        {!! Form::text('transport_trainer_cost', 250, ['class' => 'form-control', 'id' => 'transport_trainer_cost']) !!}
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group mb-3">
                                        {!! Form::label('Trainer total cost') !!}
                                        {!! Form::text('trainer_total_cost', null, ['class' => 'form-control is-valid', 'id' => 'trainer_total_cost']) !!}
                                    </div>
                                </div>
                            </div>
                            <!-- end trainer cost -->
                            <div class="dropdown-divider"></div>
                            <!-- Coordinator Cost -->
                            <h4 class="header-title mb-3 mt-4">Coordinator Cost</h4>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group mb-3">
                                        {!! Form::label('Coordinator ticket cost') !!}
                                        {!! Form::text('coordinator_ticket_cost', 800, ['class' => 'form-control', 'id' => 'coordinator_ticket_cost']) !!}
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group mb-3">
                                        {!! Form::label('Coordinator accommodation') !!}
                                        {!! Form::text('coordinator_accommodation', 1200, ['class' => 'form-control', 'id' => 'coordinator_accommodation']) !!}
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group mb-3">
                                        {!! Form::label('Coordinator transport cost') !!}
                                        {!! Form::text('transport_coordinator', 200, ['class' => 'form-control', 'id' => 'transport_coordinator']) !!}
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group mb-3">
                                        {!! Form::label('Coordinator cost') !!}
                                        {!! Form::text('coordinator_cost', 450, ['class' => 'form-control is-valid', 'id' => 'coordinator_cost']) !!}
                                    </div>
                                </div>
                            </div>
                            <!-- end Coordinator cost -->

                            <div class="dropdown-divider"></div>
                            <!-- Extra Cost -->
                            <h4 class="header-title mb-3 mt-4">Extar Cost</h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group mb-3">
                                    {!! Form::label('Bags & certificates cost') !!}
                                    {!! Form::text('bags_certificates_cost', 700, ['class' => 'form-control', 'id' => 'bags_certificates_cost']) !!}
                                    </div>
                                </div>
                            </div>
                            <!-- end Extra cost -->
                               
                            <div class="dropdown-divider"></div>
                            <h4 class="header-title mb-3 mt-4">Total Cost</h4>
                            <!-- row -->
                            <div class="row ">
                                <div class="col-md-6 card-header">
                                    <div class="form-group mb-3">
                                        {!! Form::label('Total cost in jeddah') !!}
                                        {!! Form::text('total_cost_in_jeddah', null, ['class' => 'form-control', 'id' => 'total_cost_in_jeddah']) !!}
                                    </div>
                                </div>

                                <div class="col-md-6 card-header">
                                    <div class="form-group mb-3">
                                        {!! Form::label('Total cost out jeddah') !!}
                                        {!! Form::text('total_cost_out_jeddah', null, ['class' => 'form-control', 'id' => 'total_cost_out_jeddah']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="third">
                            <!-- row -->
                            <div class="row">
                                <div class="col-md-6">
                                <h5 class="text-uppercase bg-light p-2"> Price in Jeddah</h5>
                                    <div class="form-group mb-3">
                                        {!! Form::label('Profit ratio%') !!}
                                        {!! Form::text('profit_ratio_in_jeddah', 70, ['class' => 'form-control', 'id' => 'profit_ratio_in_jeddah']) !!}
                                    </div>

                                    <div class="form-group mb-3">
                                        <!-- 7150*70/100 -->
                                        {!! Form::label('Net profit') !!}
                                        {!! Form::text('net_profit_in_jeddah', null, ['class' => 'form-control', 'id' => 'net_profit_in_jeddah']) !!}
                                    </div>

                                    <div class="form-group mb-3">
                                        {!! Form::label('Price per day') !!}
                                        {!! Form::text('price_per_day_in_jeddah', null, ['class' => 'form-control', 'id' => 'price_per_day_in_jeddah']) !!}
                                    </div>

                                    <div class="form-group mb-3">
                                        {!! Form::label('Total price') !!}
                                        {!! Form::text('total_price_in_jeddah', null, ['class' => 'form-control', 'id' => 'total_price_in_jeddah']) !!}
                                    </div>

                                    <div class="form-group mb-3">
                                        {!! Form::label('Total price after commission') !!}
                                        {!! Form::text('total_price_after_commission_in_jeddah', null, ['class' => 'form-control', 'id' => 'total_price_after_commission_in_jeddah']) !!}
                                    </div>
                                </div>

                                <div class="col-md-6">
                                <h5 class="text-uppercase bg-light p-2"> Price outside Jeddah</h5>
                                    <div class="form-group mb-3">
                                        {!! Form::label('Profit ratio%') !!}
                                        {!! Form::text('profit_ratio_out_jeddah', 70, ['class' => 'form-control', 'id' => 'profit_ratio_out_jeddah']) !!}
                                    </div>

                                    <div class="form-group mb-3">
                                        {!! Form::label('Net profit') !!}
                                        {!! Form::text('net_profit_out_jeddah', null, ['class' => 'form-control', 'id' => 'net_profit_out_jeddah']) !!}
                                    </div>

                                    <div class="form-group mb-3">
                                        {!! Form::label('Price per day') !!}
                                        {!! Form::text('price_per_day_out_jeddah', null, ['class' => 'form-control', 'id' => 'price_per_day_out_jeddah']) !!}
                                    </div>

                                    <div class="form-group mb-3">
                                        {!! Form::label('Total price') !!}
                                        {!! Form::text('total_price_out_jeddah', null, ['class' => 'form-control', 'id' => 'total_price_out_jeddah']) !!}
                                    </div>

                                    <div class="form-group mb-3">
                                        {!! Form::label('Total price after commission') !!}
                                        {!! Form::text('total_price_after_commission_out_jeddah', null, ['class' => 'form-control', 'id' => 'total_price_after_commission_out_jeddah']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end tab-content -->
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="text-center mb-3">
            <button type="submit" class="btn btn-success waves-effect waves-light">
                <span class="btn-label"><i class="mdi mdi-check-all"></i></span>Save
            </button>
        </div>
    </div> <!-- end col -->
</div>
{!! Form::close() !!}
@endsection

@section('script')
<script>
$(function(){
    // $('.trainers').select2();
    $('.courses').select2();
    $('#companies').select2();

    $.ajax({
    url: "/get-trainers",
    data: {course_id:$('.courses:last').val()},
    method: 'POST',
    success: function(result){
        for(let i = 0; i < result.length; i++)
        {
            let text = result[i].name;
            let value = result[i].id;

            var option = new Option(text, value);
            $(".trainers").append(text + ', ');
            // $(".trainers").append(option).trigger('change');
            // $('.trainers > option').prop("selected", true);
        }
    }});
    
    let service_cost = 
    `◦ 6500 ريال لليوم الواحد للدورة إجمالي 13000 ريال </br>
        ◦ التكلفة تشمل:</br>
        ◦ الحقائب التدريبية</br>
        ◦ المدرب</br>
        ◦ الشهادات التي تحتوي على رقم اعتماد من المؤسسة العامة للتدريب التقني والمهني.</br>`;
    $('#service_cost').summernote('code', service_cost);

    let payment_policy = 
    `◦ يتم سداد 50% من قيمة العقد عند التوقيع وذلك لحجز مواعيد المدرب المطلوبة </br>
    • يتم استكمال باقي الدفعة 50% قبل تنفيذ الدورة بيومين.</br>
    • في حال تم إلغاء البرنامج التدريبي من قبل العميل قبل 10 أيام من موعد التنفيذ يلتزم العميل بدفع قيمة 50% من أجمالي الرسوم.</br>
    • يسرى هذا العرض لمدة 10 أيام من تاريخه</br>`;
    $('#payment_policy').summernote('code', payment_policy);

    let general_provisions = 
    `• ازدهار تدرك الحاجة الماسة لتوفير السرية التامة للعملاء أثناء تقديم الخدمات الاستشارية والتدريبية لذا يجب أن تظل النقاشات والمداولات والشروط الخاصة بجميع العقود خاصة بكلا الطرفين ولا يجوز إفشاؤها بأية حال لأي طـــرف خارجي.</br>
    • يجب أن يخضع أي تغيير في نطاق العمل لإعادة مناقشة تكلفة العقد وبناءً عليه يتم تحديد التكلفة الجديدة.</br>
    • إننا نهدف إلى تقديم خدمة عالية الجودة تفي بمتطلباتكم فإذا رغبتم في أي وقت النقاش معنا حول كيفية تحسين خدماتنا المقدمة لكم أو كنتم غير راضيين بأي شكل عن الخدمة المقدمة لكم يرجى عدم التردد في إخطارنا بذلك.</br>
    تعهد الحفاظ على الحقوق العلمية والفكرية والأدبية للمادة التدريبية، تتعّهد الجهة الطالبة لتدريب منسوبيها بعدم إعادة التدريب بإستخدام هذه المادة أو أي جزء منها سواء كانت بالإشارة إلى المادة أو بدونه، وعليه يجب أخذ التعهدات الخطية اللازمة على جميع موظفيها الُمشاركين "الُمتدربين" للإلتزام بهذا التعّهد لحماية الحقوق العلمية والفكرية والأدبية، حيث سيتم توزيع الحقيبة التدريبية مطبوعة للُمتدربين، دون المساس بالحقوق العلمية والفكرية للمادة التدريبية والخاصة حصريا ًبمركز ازدهار للتدريب؛ كما يُمنع نسخها أو تدويرها أو توزيعها ولا حتى بشاكل جزئي ولا بأي شاكل أو وسيلة كانت بدون موافقة خطية رسمية " ُمسبقة" من•
قبل ازدهار، والله ولي التوفيق وخير الشااهدين.
جرى التوقيع لتأكيد الفهم والقبول للإلتزام بما تم توضيحه بعاليه، وما تحمله من مسؤولية وأمانة مهنية وشارعية وقانونية حسب النظام، وعليه تم التوقيع والختم بالرضى والقبول.`;
    $('#general_provisions').summernote('code', general_provisions);
});

// calculate total trainer cost
$('#trainer_cost_daily, #number_of_trainees, #number_of_days, #trainer_ticket_cost, #trainer_accommodation, #transport_trainer_cost, #coordinator_ticket_cost, #coordinator_accommodation, #transport_coordinator, #coordinator_cost, #bags_certificates_cost, #profit_ratio_out_jeddah, #profit_ratio_in_jeddah').keyup(function(event){
    // main inputs
    let trainer_cost_daily = parseInt($('#trainer_cost_daily').val());
    let number_of_trainees = parseInt($('#number_of_trainees').val());
    let number_of_days = parseInt($('#number_of_days').val());
    $('#trainer_total_cost').val(trainer_cost_daily * number_of_days);

    // trainer
    let trainer_ticket_cost = parseInt($('#trainer_ticket_cost').val());
    let trainer_accommodation = parseInt($('#trainer_accommodation').val());
    let transport_trainer_cost = parseInt($('#transport_trainer_cost').val());
    
    // coordinator
    let coordinator_ticket_cost = parseInt($('#coordinator_ticket_cost').val());
    let coordinator_accommodation = parseInt($('#coordinator_accommodation').val());
    let transport_coordinator = parseInt($('#transport_coordinator').val());
    let coordinator_cost = parseInt($('#coordinator_cost').val());

    // extra
    let bags_certificates_cost = parseInt($('#bags_certificates_cost').val());

    // result
    $('#total_cost_in_jeddah').val(
        parseInt($('#trainer_total_cost').val()) +
        coordinator_cost +
        bags_certificates_cost);

    $('#total_cost_out_jeddah').val(
        trainer_ticket_cost +
        trainer_accommodation +
        transport_trainer_cost +
        parseInt($('#trainer_total_cost').val()) +
        coordinator_ticket_cost +
        coordinator_accommodation +
        transport_coordinator +
        coordinator_cost + bags_certificates_cost);
        
    // start pricing in jeddah;    
    let profit_ratio_in_jeddah = parseInt($('#profit_ratio_in_jeddah').val());
    let net_profit_in_jeddah = $('#net_profit_in_jeddah');
    let total_price_in_jeddah = $('#total_price_in_jeddah');
    let price_per_day_in_jeddah = $('#price_per_day_in_jeddah');
    let total_price_after_commission_in_jeddah = $('#total_price_after_commission_in_jeddah');

    net_profit_in_jeddah.val(parseInt($('#total_cost_in_jeddah').val() * profit_ratio_in_jeddah / 100));
    total_price_in_jeddah.val(parseInt($('#total_cost_in_jeddah').val()) + parseInt(net_profit_in_jeddah.val()));
    price_per_day_in_jeddah.val(Math.round(parseInt(total_price_in_jeddah.val()) / number_of_days));
    total_price_after_commission_in_jeddah.val(parseInt(total_price_in_jeddah.val()));

    // start pricing out jeddah
    let profit_ratio_out_jeddah = parseInt($('#profit_ratio_out_jeddah').val());
    let net_profit_out_jeddah = $('#net_profit_out_jeddah');
    let total_price_out_jeddah = $('#total_price_out_jeddah');
    let price_per_day_out_jeddah = $('#price_per_day_out_jeddah');
    let total_price_after_commission_out_jeddah = $('#total_price_after_commission_out_jeddah');

    net_profit_out_jeddah.val(parseInt($('#total_cost_out_jeddah').val() * profit_ratio_out_jeddah / 100));
    total_price_out_jeddah.val(parseInt($('#total_cost_out_jeddah').val()) + parseInt(net_profit_out_jeddah.val()));
    price_per_day_out_jeddah.val(Math.round(parseInt(total_price_out_jeddah.val()) / number_of_days));
    total_price_after_commission_out_jeddah.val(parseInt(total_price_out_jeddah.val()));
});

// get trainers
$('.courses').change(function(e){
    $(this).closest('div.col-md-6').next().find('select').select2();
    $(this).closest('div.col-md-6').next().find('select').select2('data', null);
    $(this).closest('div.col-md-6').next().find('select').empty().trigger("change");
    $.ajax({
    url: "/get-trainers",
    data: {course_id:$(this).val()},
    method: 'POST',
    success: (result) =>{
        $(this).closest('div.col-md-6').next().find('p').html('');
        for(let i = 0; i < result.length; i++)
        {
            let text = result[i].name;
            let value = result[i].id;
            $(this).closest('div.col-md-6').next().find('p').append(text + ', ');
            // var option = new Option(text, value);
            // $(this).closest('div.col-md-6').next().find('select').append(option);

        }
    }});
    let course_id = $(this).val();
    $($(this).closest('div.col-md-6').next().find('select')).on('select2:select', function (e) {
        var data = e.params.data;
        $.ajax({
        url: "/post-trainers-to-course",
        data: {course_id:course_id, trainer_id:data.id},
        method: 'POST',
        success: (response) =>{
            console.log(response);
        }});
    });

});

// add new course and trainers
$('#add-course').click(function(){
    // $('#course-list').find(".trainers").select2();
    $('#course-list').find(".courses").select2();

    // $('#course-list').find(".trainers").select2('destroy');
    $('#course-list').find(".courses").select2('destroy');
    // $('#new-course').find(".trainers").select2('destroy');
    $('#new-course').find(".courses").select2('destroy');

    $course_input = $('#course-list').clone(true, true).first();

    $('#new-course').append($course_input);
    // $('#new-course').find(".trainers").select2();
    $('#new-course').find(".courses").select2();

    

});
</script>
@endsection