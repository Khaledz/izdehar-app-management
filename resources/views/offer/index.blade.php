@extends('layouts.app')
@section('title')
    List Offers
@endsection
@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Course</th>
                            <th>Company</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if($offers->count() > 0)
                        @foreach($offers as $offer)
                        <tr>
                            <td>{{ $offer->title }}</td>
                            <td>
                            @foreach($offer->courses as $course)
                                {{ $course->title }}
                            @endforeach
                            </td>
                            <td>{{ $offer->company->name }}</td>
                            <td>{{ $offer->status->name }} 
                            <a href="{{ url('change-offer-status', $offer->id)}}">Change</a>
                            </td>
                            <td>
                            <a href="{{ route('offer.show', $offer->id)}}" class="btn btn-xs btn-info"><i class="mdi mdi-eye"></i></a>
                            <a href="{{ route('offer.edit', $offer->id)}}" class="btn btn-xs btn-secondary"><i class="mdi mdi-pencil"></i></a>
                            <a href="#" class="btn btn-xs btn-danger" onclick="document.getElementById('deleteForm').submit();"><i class="mdi mdi-delete"></i></a>
                            {!! Form::open(['route' => ['offer.destroy', $offer], 'method' => 'delete', 'id' => 'deleteForm']) !!}
                            {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
                                        
@endsection

@section('script')
<script src="/assets/libs/datatables/jquery.dataTables.js"></script>
<script src="/assets/libs/datatables/dataTables.bootstrap4.js"></script>
<script src="/assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="/assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="/assets/libs/datatables/buttons.html5.min.js"></script>
<script src="/assets/js/pages/datatables.init.js"></script>
@endsection