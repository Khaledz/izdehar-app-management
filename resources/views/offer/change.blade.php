@extends('layouts.app')
@section('title')
    Change status of {{ $offer->title }}
@endsection

@section('content')

<!-- end page title --> 
{!! Form::model($offer, ['route' => ['offer.change', $offer], 'method' => 'patch']) !!}
<div class="row">
    <div class="col-lg-12">
        <div class="card-box">

            <div class="form-group mb-3">
                {!! Form::label('Offer Status') !!}
                {!! Form::select('offer_status_id', $statuses, null, ['class' => 'select2 form-control', 'id' => 'courseType']) !!}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="text-center mb-3">
            <button type="submit" class="btn btn-success waves-effect waves-light">
                <span class="btn-label"><i class="mdi mdi-check-all"></i></span>Save
            </button>
        </div>
    </div> <!-- end col -->
</div>

{!! Form::close() !!}
@endsection

