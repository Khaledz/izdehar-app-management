@extends('layouts.app')
@section('title')
    Create Advertisement
@endsection

@section('content')

<!-- end page title --> 
{!! Form::open(['route' => 'ad.store', 'method' => 'post', 'files' => true, 'name' => 'form']) !!}
<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
        <h4 class="header-title mb-3">Advertisement Details</h4>
            <div class="form-group mb-3">
                {!! Form::label('Title') !!}
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('Content') !!}
                {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => 4]) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('Attachment (optional)') !!}
                {!! Form::file('attach',  ['class' => 'form-control']) !!}
            </div>
            
        </div> <!-- end card-box -->
    </div> <!-- end col -->

    <div class="col-lg-12">
        <div class="card-box">
        <h4 class="header-title mb-3">Advertisement Audience</h4>

        <p class="font-13 mb-2"><b>Channels</b></p>
            <div class="form-group mb-3">
            @foreach($channels as $channel)
                <div class="checkbox checkbox-success form-check-inline">
                    <input type="checkbox" name="channels[]" value="{{ $channel->id }}" class="changeChannel" id="{{ $channel->name }}">
                    <label>{{ $channel->name }}</label>
                </div>
            @endforeach
            </div>
            <!-- Accounts List -->
            <!-- email -->
            <div id="accounts-list">
                <div class="form-group mb-3"  id="account">
                    
                </div>
            </div>

            <!-- email -->
            <div class="row" id="showEmail">
                <div class="col-md-5 form-group mb-3">
                {!! Form::label('Emails Audience') !!}
                {!! Form::select('emails[]', $emails, null, ['class' => 'select2 form-control select2-multiple', 'id' => 'emailData', 'multiple' => 'multiple']) !!}
                </div>
                <div class="col-md-2 text-center"><h4>OR</h4></div>
                <div class="col-md-5 form-group mb-3">
                {!! Form::label('Import excel file') !!}
                {!! Form::select('emailData', $attachments, null, ['class' => 'form-control']) !!}
                <button class="btn btn-success bt-xs" id="emailImportBtn">
                    Import data
                </button>    
            </div>
            </div>
            <div class="dropdown-divider"></div>
            <!-- SMS -->
            <div class="row" id="showSms">
                <div class="col-md-5 form-group mb-3">
                {!! Form::label('SMS Audience') !!}
                {!! Form::select('phones[]', $phones, null, ['class' => 'select2 form-control select2-multiple', 'id' => 'phoneData', 'multiple' => 'multiple']) !!}
                </div>
                <div class="col-md-2 text-center"><h4>OR</h4></div>
                <div class="col-md-5 form-group mb-3">
                {!! Form::label('Import excel file') !!}
                {!! Form::select('smsData', $attachments, null, ['class' => 'form-control']) !!}
                <button class="btn btn-success bt-xs" id="smsImportBtn">
                    Import data
                </button>
                </div>
            </div>

            <!-- publish -->
            <div class="form-group mb-3">
                {!! Form::label('When to publish this advertisement?') !!}
                <div class="radio mt-1">
                    <input id="inlineRadio1" type="radio" value="now" name="publish" checked>
                    <label for="inlineRadio1"> Publish now </label>
                </div>
                <div class="radio mt-1">
                    <input id="inlineRadio2" type="radio" value="later" name="publish">
                    <label for="inlineRadio2"> Publish later </label>
                </div>
            </div>

            <!-- if later, show date and time -->
            <div class="form-group mb-3" id="toggleDateTime">
                {!! Form::label('Date & Time') !!} <b>(Time now is {{ \Carbon\Carbon::now()->format('Y-m-d H:i') }})</b>
                <input name="datetime" type="text" id="datetime-datepicker" class="form-control" placeholder="Date and Time">
            </div>
            
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<div class="row">
    <div class="col-12">
        <div class="text-center mb-3">
            <button type="submit" class="btn btn-success waves-effect waves-light">
                <span class="btn-label"><i class="mdi mdi-check-all"></i></span>Save & Publish
            </button>
        </div>
    </div> <!-- end col -->
</div>
{!! Form::close() !!}
<button type="button" class="btn btn-info btn-xs" id="sa-success" style="display: none;"></button>
@endsection

@section('script')
<script>
$(function(){
$('#accounts-list').hide();
});

$('#smsImportBtn').click(function(e){
    e.preventDefault();
    smsForm = document.forms.namedItem('form');
    formDataSMS = new FormData(smsForm);
    $.ajax({
    url: "/extarct-phones",
    data: formDataSMS,
    method: 'POST',
    dataType: 'json',
    processData: false,
    contentType: false,
    success: function(result){
        for(let i = 0; i < result.data['phone'].length; i++)
        {
            let text = result.data['phone'][i];
            let value = result.data['phone'][i];

            var option = new Option(text, value);
            $("#phoneData").append(option);
            $('#phoneData > option').prop("selected", true);
        }
        $('#sa-success').click();
    }});
});

$('#emailImportBtn').click(function(e){
    e.preventDefault();
    form = document.forms.namedItem('form');
    formData = new FormData(form);
    $.ajax({
    url: "/extarct-emails",
    data: formData,
    method: 'POST',
    dataType: 'json',
    processData: false,
    contentType: false,
    success: function(result){
        for(let i = 0; i < result.data['email'].length; i++)
        {
            let text = result.data['email'][i];
            let value = result.data['email'][i];

            var option = new Option(text, value);
            $("#emailData").append(option);
            $('#emailData > option').prop("selected", true);
        }
        $('#sa-success').click();
    }});
});


</script>
<!-- Sweet Alerts js -->
<script src="/assets/libs/sweetalert2/sweetalert2.min.js"></script>

<!-- Sweet alert init js-->
<script src="/assets/js/pages/sweet-alerts.init.js"></script>

@endsection

@section('style')
<link href="/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
@endsection