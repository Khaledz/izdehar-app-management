@extends('layouts.app')
@section('title')
    Show Advertisement
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <h3 class="text-center">{{ $ad->title }}</h3>
            @if(count($ad->attachments) > 0)
            <div class="row">
                <div class="text-center center-block">
                    <img src="{{ asset('storage/ads/' . $ad->attachments->first()->name) }}" class="rounded mr-1" height="160"/>
                </div>
            </div>
            @endif
            <h4 class="header-title mb-2">Description:</h4>
            <p>{{ $ad->description }}</p>
            <div class="dropdown-divider mt-3 mb-3"></div>
            <h4 class="header-title ">Channels:</h4>
            {{ $ad->channels()->pluck('name')->implode(', ') }}
            <div class="dropdown-divider mt-3 mb-3"></div>
            <h4 class="header-title">Receivers:</h4>
            <div class="row">
                <div class="col-md-6">
                <h5>Emails</h5>
                    <ul>
                    @if($ad->receiver->emails != null)
                        @foreach($ad->receiver->emails as $email)
                            <li>{{ $email }}</li>
                        @endforeach
                    @endif
                    </ul>
                </div>
                <div class="col-md-6">
                <h5>Phones</h5>
                    <ul>
                    @if($ad->receiver->phones != null)
                        @foreach($ad->receiver->phones as $phone)
                            <li>{{ $phone }}</li>
                        @endforeach
                    @endif
                    </ul>
                </div>
            </div>

            <div class="dropdown-divider mt-3 mb-3"></div>
            <h4 class="header-title">Errors:</h4>
                    <ul>
                    @if(\App\Models\QueueError::where('ad_id', $ad->id)->count() != 0)
                        @foreach(\App\Models\QueueError::where('ad_id', $ad->id)->get() as $error)
                            <li>{{ $error->channel }} - {{ $error->error }}</li>
                        @endforeach
                    @endif
                    </ul>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>


@endsection
