@extends('layouts.app')
@section('title')
    Create Trainer
@endsection

@section('content')

<!-- end page title --> 
{!! Form::open(['route' => 'trainer.store', 'method' => 'post', 'files' => true]) !!}
<div class="row">
    <div class="col-lg-12">
        <div class="card-box">

            <div class="form-group mb-3">
                {!! Form::label('Name') !!}
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('Email') !!}
                {!! Form::text('email', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('Phone') !!}
                {!! Form::text('phone', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('Resume') !!}
                {!! Form::file('resume',  ['class' => 'form-control']) !!}
            </div>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<div class="row">
    <div class="col-12">
        <div class="text-center mb-3">
            <button type="submit" class="btn btn-success waves-effect waves-light">
                <span class="btn-label"><i class="mdi mdi-check-all"></i></span>Save
            </button>
        </div>
    </div> <!-- end col -->
</div>
{!! Form::close() !!}
@endsection
