@extends('layouts.app')
@section('title')
    List Users
@endsection
@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if($users->count() > 0)
                        @foreach($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->roles->first() != null ? $user->roles->first()->name : '' }}</td>
                            <td>
                            <a href="{{ route('user.show', $user->id)}}" class="btn btn-xs btn-info"><i class="mdi mdi-eye"></i></a>
                            <a href="{{ route('user.edit', $user->id)}}" class="btn btn-xs btn-secondary"><i class="mdi mdi-pencil"></i></a>
                            <a href="#" class="btn btn-xs btn-danger" onclick="document.getElementById('deleteForm').submit();"><i class="mdi mdi-delete"></i></a>
                            {!! Form::open(['route' => ['user.destroy', $user], 'method' => 'delete', 'id' => 'deleteForm']) !!}
                            {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
                                        
@endsection

@section('script')
<script src="/assets/libs/datatables/jquery.dataTables.js"></script>
<script src="/assets/libs/datatables/dataTables.bootstrap4.js"></script>
<script src="/assets/libs/datatables/dataTables.buttons.min.js"></script>
<script src="/assets/libs/datatables/buttons.bootstrap4.min.js"></script>
<script src="/assets/libs/datatables/buttons.html5.min.js"></script>
<script src="/assets/js/pages/datatables.init.js"></script>
@endsection