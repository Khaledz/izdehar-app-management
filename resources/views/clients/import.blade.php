@extends('layouts.app')
@section('title')
    Import Clients from Excel
@endsection

@section('content')

<!-- end page title --> 
{!! Form::open(['route' => 'client.import.store', 'method' => 'post', 'files' => true]) !!}
<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
        <p>This tool enables you to import clients from existing excel file automatically.</p>
            <div class="form-group mb-3">
                {!! Form::label('Import excel file') !!}
                {!! Form::file('excel_file', ['class' => 'form-control']) !!}
            </div>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<div class="row">
    <div class="col-12">
        <div class="text-center mb-3">
            <button type="submit" class="btn btn-success waves-effect waves-light">
                <span class="btn-label"><i class="mdi mdi-check-all"></i></span>Upload & Save
            </button>
        </div>
    </div> <!-- end col -->
</div>
{!! Form::close() !!}
@endsection
