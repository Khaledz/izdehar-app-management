@extends('layouts.app')
@section('title')
    Edit Advertisement
@endsection

@section('content')

<!-- end page title --> 
{!! Form::model($ad, ['route' => ['ad.update', $ad], 'method' => 'patch']) !!}
<div class="row">
    <div class="col-lg-12">
        <div class="card-box">

            <div class="form-group mb-3">
                {!! Form::label('Title') !!}
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group mb-3">
                {!! Form::label('Content') !!}
                {!! Form::textarea('desription', null, ['class' => 'form-control', 'rows' => 4]) !!}
            </div>

            <div class="form-group mb-3">
            @foreach($channels as $channel)
                <div class="checkbox checkbox-success form-check-inline">
                    {!! Form::checkbox('channels[]', $channel->id) !!}
                    <label>{{ $channel->name }}</label>
                </div>
            @endforeach
            </div>

        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<div class="row">
    <div class="col-12">
        <div class="text-center mb-3">
            <button type="submit" class="btn btn-success waves-effect waves-light">
                <span class="btn-label"><i class="mdi mdi-check-all"></i></span>Save
            </button>
        </div>
    </div> <!-- end col -->
</div>
{!! Form::close() !!}
@endsection
