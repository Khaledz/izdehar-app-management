$(document).ready(function() {
    // $('#select1').select2();
    // $('#select2').select2();
    $('.select2').select2();
    $('#toggleDateTime').hide();
    $('#showEmail').hide();
    $('#showSms').hide();
});

! function(e) {
    "use strict";
    var i = function() {};
    i.prototype.init = function() {
        e("#basic-datepicker").flatpickr(), e("#datetime-datepicker").flatpickr({
            enableTime: !0,
            dateFormat: "Y-m-d H:i"
        }), e("#humanfd-datepicker").flatpickr({
            altInput: !0,
            altFormat: "F j, Y",
            dateFormat: "Y-m-d"
        }), e("#minmax-datepicker").flatpickr({
            minDate: "2020-01",
            maxDate: "2020-03"
        }), e("#disable-datepicker").flatpickr({
            onReady: function() {
                this.jumpToDate("2025-01")
            },
            disable: ["2025-01-10", "2025-01-21", "2025-01-30", new Date(2025, 4, 9)],
            dateFormat: "Y-m-d"
        }), e("#multiple-datepicker").flatpickr({
            mode: "multiple",
            dateFormat: "Y-m-d"
        }), e("#conjunction-datepicker").flatpickr({
            mode: "multiple",
            dateFormat: "Y-m-d",
            conjunction: " :: "
        }), e("#range-datepicker").flatpickr({
            mode: "range"
        }), e("#inline-datepicker").flatpickr({
            inline: !0
        }), e("#basic-timepicker").flatpickr({
            enableTime: !0,
            noCalendar: !0,
            dateFormat: "H:i"
        }), e("#24hours-timepicker").flatpickr({
            enableTime: !0,
            noCalendar: !0,
            dateFormat: "H:i",
            time_24hr: !0
        }), e("#minmax-timepicker").flatpickr({
            enableTime: !0,
            noCalendar: !0,
            dateFormat: "H:i",
            minDate: "16:00",
            maxDate: "22:30"
        }), e("#preloading-timepicker").flatpickr({
            enableTime: !0,
            noCalendar: !0,
            dateFormat: "H:i",
            defaultDate: "01:45"
        }), e("#basic-colorpicker").colorpicker(), e("#hexa-colorpicker").colorpicker({
            format: "auto"
        }), e("#component-colorpicker").colorpicker({
            format: null
        }), e("#horizontal-colorpicker").colorpicker({
            horizontal: !0
        }), e("#inline-colorpicker").colorpicker({
            color: "#DD0F20",
            inline: !0,
            container: !0
        }), e(".clockpicker").clockpicker({
            donetext: "Done"
        }), e("#single-input").clockpicker({
            placement: "bottom",
            align: "left",
            autoclose: !0,
            default: "now"
        }), e("#check-minutes").click(function(i) {
            i.stopPropagation(), e("#single-input").clockpicker("show").clockpicker("toggleView", "minutes")
        })
    }, e.FormPickers = new i, e.FormPickers.Constructor = i
}(window.jQuery),
function(e) {
    "use strict";
    e.FormPickers.init()
}(window.jQuery);

$('input[name=publish]').on('change', function() {
    if($(this).val() == 'now')
    {
        $('#toggleDateTime').hide();
    }
    else if ($(this).val() == 'later')
    {
        $('#toggleDateTime').show();
    }
});

$('.changeChannel').on('change', function() {
    let type = $(this).val();
    let isChecked = $(this).prop('checked');
    $.ajax({
        url: "/getAccountsByType",
        data: {channel:type},
        method: 'POST',
        success: function(result){
            $('#accounts-list').show();
            for(let i = 0; i < result.length; i++) 
            {
                let html = '<div class="checkbox checkbox-success form-check-inline"><input type="checkbox" name="accounts[]" value="'+ result[i].id +'" class=""><label>'+ result[i].name +'</label></div>';
                $('#account').append(html);
            }
        }
    });

    if($(this).val() == 5 && $(this).is(':checked'))
    {
        $('#showEmail').show();
    }
    else if($(this).val() == 6 && $(this).is(':checked'))
    {
        $('#showSms').show();
    }
    else if($(this).val() == 5 && ! $(this).is(':checked'))
    {
        $('#showEmail').hide();
    }
    else if($(this).val() == 6 && ! $(this).is(':checked'))
    {
        $('#showSms').hide();
    }
});

// $('#smsSelect').on('change', function(){
//         let data = '{{ session("emails") }}';
//         console.log(data);
//         var option = new Option("option text", "value");
//         $(option).html("option text");
//         $("#select2").append(o);
//     });