<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Bills extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('costs', function(Blueprint $table) {
            $table->increments('id');
            $table->string('coach_wage')->nullable();
            $table->string('coach_number')->nullable();
            $table->string('days')->nullable();
            $table->string('coach_ticket_cost')->nullable();
            $table->string('coordinator_ticket_cost')->nullable();
            $table->string('coach_total_cost')->nullable();
            $table->string('bags_certificates_cost')->nullable();
            $table->string('coordinator_cost')->nullable();
            $table->string('coach_accommodation')->nullable();
            $table->string('coordinator_accommodation')->nullable();
            $table->string('transport_coach_coordinator')->nullable();
            $table->string('total_cost_in_jeddah')->nullable();
            $table->string('total_cost_out_jeddah')->nullable();
            $table->string('profit_ratio_in_jeddah')->nullable();
            $table->string('total_price_in_jeddah')->nullable();
            $table->string('total_price_after_commission_in_jeddah')->nullable();
            $table->string('price_per_day_in_jeddah')->nullable();
            $table->string('net_profit_in_jeddah')->nullable();
            $table->string('profit_ratio_out_jeddah')->nullable();
            $table->string('total_price_out_jeddah')->nullable();
            $table->string('total_price_after_commission_out_jeddah')->nullable();
            $table->string('price_per_day_out_jeddah')->nullable();
            $table->string('net_profit_out_jeddah')->nullable();
            $table->integer('offer_id')->unsigned();
            $table->timestamps();

            $table->foreign('offer_id')->references('id')->on('offers')->onDelete('cascade');
        });

        Schema::create('bill_types', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('bills', function(Blueprint $table) {
            $table->increments('id');
            $table->string('bill_number');
            $table->string('tax_number')->nullable();
            $table->date('date');
            $table->string('model')->nullable();
            $table->string('model_id')->nullable();
            $table->integer('offer_id')->unsigned();
            $table->integer('bill_type_id')->unsigned();
            $table->timestamps();

            $table->foreign('offer_id')->references('id')->on('offers')->onDelete('cascade');
            $table->foreign('bill_type_id')->references('id')->on('bill_types')->onDelete('cascade');
        });

        Schema::create('payments', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('total');
            $table->integer('paid_amount')->default(0);
            $table->string('first_paid_amount_percentage');
            $table->integer('remaining_amount')->default(0);
            $table->boolean('is_paid')->default(0);
            $table->integer('bill_id')->unsigned();
            $table->timestamps();

            $table->foreign('bill_id')->references('id')->on('bills')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
