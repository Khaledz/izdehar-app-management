<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_channels', function(Blueprint $table) {
            $table->increments('id');
            $table->text('name');
        });

        Schema::create('ads', function(Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->longText('description')->nullable();
            $table->boolean('is_publish')->default(0);
            $table->string('datetime')->nullable();
            $table->timestamps();
        });

        Schema::create('ad_receivers', function(Blueprint $table) {
            $table->integer('ad_id')->unsigned();
            $table->json('emails')->nullable();
            $table->json('phones')->nullable();
            $table->timestamps();

            $table->foreign('ad_id')->references('id')->on('ads')->onDelete('cascade');
        });

        Schema::create('ads_channels', function(Blueprint $table) {
            $table->integer('ad_id')->unsigned();
            $table->integer('ad_channel_id')->unsigned();

            $table->foreign('ad_id')->references('id')->on('ads')->onDelete('cascade');
            $table->foreign('ad_channel_id')->references('id')->on('ad_channels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_channels');
        Schema::dropIfExists('ads');
        Schema::dropIfExists('ads_channels');
    }
}
