<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SocialMediaAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_providers', function(Blueprint $table) {
            $table->increments('id');
            $table->text('name');
        });

        Schema::create('accounts', function(Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->longText('username')->nullable();
            $table->longText('password')->nullable();
            $table->longText('token')->nullable();
            $table->string('expire')->default('123456');
            $table->longText('app_id')->nullable();
            $table->longText('app_secret')->nullable();
            $table->longText('page_id')->nullable();
            $table->integer('account_provider_id')->unsigned();
            $table->timestamps();

            $table->foreign('account_provider_id')->references('id')->on('account_providers')->onDelete('cascade');
        });

        Schema::create('qeue_errors', function(Blueprint $table) {
            $table->increments('id');
            $table->text('channel')->nullable();
            $table->text('error')->nullable();
            $table->text('ad_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_providers');
        Schema::dropIfExists('accounts');
    }
}
