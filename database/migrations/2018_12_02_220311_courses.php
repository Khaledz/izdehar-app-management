<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Courses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_types', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('courses', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('description');
            $table->integer('days')->nullable();
            $table->string('city')->nullable();            
            $table->integer('course_type_id')->unsigned();
            $table->timestamps();

            $table->foreign('course_type_id')->references('id')->on('course_types')->onDelete('cascade');
        });

        Schema::create('course_dates', function(Blueprint $table) {
            $table->integer('course_id')->unsigned();
            $table->timestamp('datetime');

            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
        });

        Schema::create('courses_trainers', function(Blueprint $table) {
            $table->integer('course_id')->unsigned();
            $table->integer('trainer_id')->unsigned();

            $table->foreign('trainer_id')->references('id')->on('trainers')->onDelete('cascade');
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
        });

        Schema::create('courses_clients', function(Blueprint $table) {
            $table->integer('course_id')->unsigned();
            $table->integer('client_id')->unsigned();

            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
        });

        Schema::create('offer_statuses', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('offers', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('days_number');
            $table->integer('hours_per_day');
            $table->string('location');
            $table->longText('program_idea')->nullable();
            $table->string('program_duration')->nullable();
            $table->string('program_accreditation')->nullable();
            $table->longText('program_execution')->nullable();
            $table->integer('trainer_cost_per_day');
            $table->integer('number_of_trainees');
            $table->integer('trainer_ticket_cost');
            $table->integer('trainer_accommodation');
            $table->integer('transport_trainer_cost');
            $table->integer('trainer_total_cost');
            $table->integer('coordinator_ticket_cost');
            $table->integer('coordinator_accommodation');
            $table->integer('transport_coordinator');
            $table->integer('coordinator_cost');
            $table->integer('bags_certificates_cost');
            $table->integer('total_cost_in_jeddah');
            $table->integer('total_cost_out_jeddah');
            $table->integer('profit_ratio_in_jeddah');
            $table->integer('net_profit_in_jeddah');
            $table->integer('net_profit_out_jeddah');
            $table->integer('price_per_day_in_jeddah');
            $table->integer('total_price_in_jeddah');
            $table->integer('total_price_after_commission_in_jeddah');
            $table->integer('profit_ratio_out_jeddah');
            $table->integer('price_per_day_out_jeddah');
            $table->integer('total_price_out_jeddah');
            $table->integer('total_price_after_commission_out_jeddah');
            $table->longText('service_cost');
            $table->longText('payment_policy');
            $table->longText('general_provisions');
            $table->integer('company_id')->unsigned();
            $table->integer('offer_status_id')->unsigned()->default(1);
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->foreign('offer_status_id')->references('id')->on('offer_statuses')->onDelete('cascade');
        });

        Schema::create('offers_courses', function(Blueprint $table) {
            $table->integer('course_id')->unsigned();
            $table->integer('offer_id')->unsigned();

            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
            $table->foreign('offer_id')->references('id')->on('offers')->onDelete('cascade');
        });
        
        // trainers might be changed in the offer, example not all trainers 
        // associated to the course are available in the offer. so maybe some of them.
        Schema::create('offers_trainers', function(Blueprint $table) {
            $table->integer('trainer_id')->unsigned();
            $table->integer('offer_id')->unsigned();

            $table->foreign('trainer_id')->references('id')->on('trainers')->onDelete('cascade');
            $table->foreign('offer_id')->references('id')->on('offers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
        Schema::dropIfExists('courses_clients');
        Schema::dropIfExists('payments');
    }
}
