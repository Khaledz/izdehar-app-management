<?php

use Illuminate\Database\Seeder;

class SetupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->insert([
            'name' => 'Admin',
            'guard_name' => 'web'
        ]);

        \DB::table('roles')->insert([
            'name' => 'Sales',
            'guard_name' => 'web'
        ]);

        \DB::table('roles')->insert([
            'name' => 'Accountant',
            'guard_name' => 'web'
        ]);

        \DB::table('roles')->insert([
            'name' => 'Designer',
            'guard_name' => 'web'
        ]);

        \DB::table('roles')->insert([
            'name' => 'Training Coordinator',
            'guard_name' => 'web'
        ]);
        
        \DB::table('model_has_roles')->insert([
            'role_id' => 1,
            'model_type' => 'App\Models\User',
            'model_id' => 1
        ]);

        \DB::table('ad_channels')->insert([
            'name' => 'Facebook',
        ]);

        \DB::table('ad_channels')->insert([
            'name' => 'Twitter',
        ]);

        \DB::table('ad_channels')->insert([
            'name' => 'Instagram',
        ]);

        \DB::table('ad_channels')->insert([
            'name' => 'Linkedin',
        ]);

        \DB::table('ad_channels')->insert([
            'name' => 'Email',
        ]);

        \DB::table('ad_channels')->insert([
            'name' => 'SMS',
        ]);

        \DB::table('customer_types')->insert([
            'name' => 'Gold Customer',
        ]);

        \DB::table('customer_types')->insert([
            'name' => 'Default Customer',
        ]);

        \DB::table('course_types')->insert([
            'name' => 'Public Course',
        ]);

        \DB::table('course_types')->insert([
            'name' => 'In-house Course',
        ]);

        \DB::table('offer_statuses')->insert([
            'name' => 'Waiting for approval',
        ]);

        \DB::table('offer_statuses')->insert([
            'name' => 'Waiting for payment',
        ]);

        \DB::table('offer_statuses')->insert([
            'name' => 'Paid',
        ]);

        \DB::table('offer_statuses')->insert([
            'name' => 'Done',
        ]);

        \DB::table('account_providers')->insert([
            'name' => 'Facebook',
        ]);

        \DB::table('account_providers')->insert([
            'name' => 'Twitter',
        ]);

        \DB::table('account_providers')->insert([
            'name' => 'Instagram',
        ]);

        \DB::table('account_providers')->insert([
            'name' => 'Linkedin',
        ]);

        \DB::table('bill_types')->insert([
            'name' => 'Company Bill',
        ]);

        \DB::table('bill_types')->insert([
            'name' => 'Client Bill',
        ]);
    }
}
