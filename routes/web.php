<?php
session_start();
ini_set('memory_limit', '-1');
use LinkedIn\Client;
use LinkedIn\Scope;
use LinkedIn\AccessToken;
use Carbon\Carbon;

Auth::loginUsingId(1);

Route::get('/linkedin', function(){
    // AQQlWxCXDY59DAISCNLC_9CgkRQY42fVyAN4qbbgwrCppJELO5TXw1WYtE3y8QDJ8WC3xRi6jO3chsCitNGwjTP_LXbpaVkep0-Mi503O62G3fsHSv8afnVpHGpAU6DXZ28jBGdyWcSbUXHD6UE6bWhNJBtzwTtW_x_S0qMstTBdB1JqZ9EPGnj11yNGIA
    // dd($_GET);
    $client = new Client(
        session('account')->username,
        session('account')->password
    );
    $client->setRedirectUrl('http://auto.localhost/linkedin');
    // retrieve access token using code provided by LinkedIn
    $accessToken = $client->getAccessToken($_GET['code']);    // $client->setRedirectUrl('http://your.domain.tld/path/to/script/');
    session(['accessToken' => json_encode($accessToken)]);
    $account = \App\Models\Account::find(session('account')->id);
    $account->token = json_decode(session('accessToken'))->token;
    $account->expire = json_decode(session('accessToken'))->expiresAt;
    $account->save();
    return back()->with('message', 'New token has been generated!');
});
Auth::routes();
/**
 * Pretty print whatever passed in
 *
 * @param mixed $anything
 */

// Auth access
Route::group(['middleware' => ['role:Admin','auth']], function(){
    Route::get('/', ['uses' => 'HomeController@index']);
    Route::get('/policy', function(){
        return view('policy');
    });
    
    Route::resource('/ad', 'AdController');
    Route::resource('/client', 'ClientController');
    Route::get('/import-data', ['uses' => 'ClientController@import', 'as' => 'client.import']);
    Route::post('/import-data', ['uses' => 'ClientController@import_store', 'as' => 'client.import.store']);

    Route::resource('/offer', 'OfferController');
    Route::get('/change-offer-status/{offer}', ['uses' => 'OfferController@change_status_get']);
    Route::patch('/change-offer-status/{offer}', ['uses' => 'OfferController@change_status_post', 'as' => 'offer.change']);
    Route::resource('/company', 'CompanyController');
    Route::resource('/course', 'CourseController');
    Route::resource('/trainer', 'TrainerController');
    Route::post('/get-trainers', ['uses' => 'TrainerController@getTrainersByCourse']);
    Route::resource('/social-media-account', 'AccountController');
    Route::resource('/payment', 'PaymentController');
    Route::resource('/bill', 'BillController');
    Route::resource('/user', 'UserController');
    Route::resource('/attachment', 'AttachmentController');

    // import excel files in ad
    Route::post('/extarct-emails', ['uses' => 'ImportController@import_emails_store']);
    Route::post('/extarct-phones', ['uses' => 'ImportController@import_phones_store']);

    Route::post('/post-trainers-to-course', ['uses' => 'OfferController@postTrainerToCourse']);

    Route::post('getAccountsByType', ['uses' => 'AccountController@getAccountsByType']);
});
